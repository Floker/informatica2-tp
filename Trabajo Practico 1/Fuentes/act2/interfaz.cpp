/**
 * @file interfaz.h
 * @author Bertolin Gianfranco (gianfrancobertolin@alu.frp.utn.edu.ar)
 * @brief Este header define la clase de la interfaz de consola
 * @version 0.1
 * @date 2021-07-30
 * @copyright MIT Licence 2021
 *
 */

#include "interfaz.h"
#include "eqsys.h"
#include "cramer.h"
#include "gaussel.h"
#include <iostream>
#include <string>
#include <cctype>
#include <cstdlib>
#include <windows.h>
using namespace std;

interfaz::interfaz()
{

}
bool interfaz::esNumero(string linea) {

    bool esEntero = true;
    int longitud = linea.size();

    if (longitud == 0) {
        esEntero = false;
    } else if (longitud == 1 && !isdigit(linea[0])) {
        esEntero = false;
    } else {
        int indice = 0;
        if (linea[0] == '+' || linea[0] == '-') {
            indice = 1;
        } else {
            indice = 0;
        }

        while (indice < longitud) {
            if (!isdigit(linea[indice])) {
                esEntero = false;
                break;
            }
            indice++;
        }
    }
    return esEntero;
}

short interfaz:: selectCarga(){

    string linea;
    int op=0;
    bool repite = true;
    fflush(stdin);
    do {
        cout << "Por favor, Ingrese el metodo de carga que desee\n"<<endl;
        cout << "\t1-  Por archivo\n"<<endl;
        cout << "\t2-  Por consola\n" << endl;
        cout<<"\t";
        getline(cin, linea);

        repite=atoi(linea.c_str())>2||atoi(linea.c_str())<=0;
        if (repite)
        {
            cout<<"\n"<<endl;
            cout << "Ingrese un valor valido"<< endl;
            cout<<"\n"<<endl;
            system("pause");
            system("cls");
        }


    }while (!esNumero(linea)||repite);

    op = atoi(linea.c_str());
    system("cls");
    return op;
}

void interfaz::mostrarSys(eqSys *sys){
    int ord;
    unsigned int coe=0;
    bool check= true;
    ord = sys->getSize();
    coe = sys->getSize();
    cout<<"\n"<<endl;

    for(int i=0;i<ord;i++){
        for(int j=0;j<(ord+1);j++){

                if(coe==0)
                {
                    if(sys->getElement(i,j)>0)
                   cout<<" = +"<<sys->getElement(i,j);

                    else if(sys->getElement(i,j)<0)
                   cout<<" = "<<sys->getElement(i,j);

                    else
                    cout<<" =  "<<sys->getElement(i,j);

                    coe=sys->getSize();
                    check=false;
                }

                       if(check)
                       {
                            if(sys->getElement(i,j)<0)
                            {
                             cout<<" "<<sys->getElement(i,j)<<" (x^"<<coe<<")";
                            }
                            else
                            {
                             cout<<" +"<<sys->getElement(i,j)<<" (x^"<<coe<<")";
                            }
                             coe=coe-1;
                        }

                       check=true;

        }
        cout<<"\n";
    }
  cout<<"\n"<<endl;


}

void interfaz::cargarSys(eqSys *sys){
    int _ord, op;
    double valor;
    string linea;
    bool repite = true;
    fflush(stdin);
    cout << "Ingrese el orden del sistema"<< endl;
     cout << "\n"<<endl;
     cout << "\t";
    cin  >> _ord;
    cout << "\n"<<endl;

    sys->setSize(_ord);

    for(int i=0;i<_ord;i++){
        for(int j=0;j<(_ord+1);j++){
            cout<<"Digite un numero ["<<i<<"]["<<j<<"]: ";
            cin>>valor;
            sys->setElement(i,j,valor);
        }
    }
     fflush(stdin);
    do{
        mostrarSys(sys);

        cout << "El sistema es correcto?\n"<<endl;
        cout << "  1-Si\n"<<endl;
        cout << "  2-No\n"<<endl;
        getline(cin, linea);

        repite=atoi(linea.c_str())>2||atoi(linea.c_str())<=0;
        if (repite)
        {
            cout<<"\n"<<endl;
            cout << "Ingrese un valor valido"<< endl;
            cout<<"\n"<<endl;
            system("pause");
            system("cls");
        }

    } while (!esNumero(linea)||repite);

    op = atoi(linea.c_str());
    system("cls");

    if(op!=1){
        cargarSys(sys);
    }

}

short interfaz::selectMetodo(unsigned int orden){
    string linea;
    int op=0;
    bool repite = true;
    fflush(stdin);
    if(orden<=3){

        cout << "Sistema de orden: "<<orden<<" \n"<< endl;
        do{

            cout << "Ingrese la opcion que desee: \n"<<endl;
            cout << "\t1-Resolver con Gauss\n";
            cout << "\t2-Resolver con Cramer\n" << endl;
            cout << "\t";

            getline(cin, linea);

            repite=atoi(linea.c_str())>2||atoi(linea.c_str())<=0;
            if (repite)
            {
                cout << "Ingrese un valor valido\n"<< endl;
                system("pause");
                system ("cls");
            }

        } while (!esNumero(linea)||repite);
        system("cls");
        op = atoi(linea.c_str());

    }
    else {
        cout << "Sistema de orden: \n"<<orden;
        cout << "\nEl sistema solo permite ser resuelto con Gauss\n\n";
        system("pause");
        system("cls");
        op = 2;
    }

    return op;
}
void interfaz::mostrarSol(eqSys *sys){
    int orden, i=0;
    float sol;

    orden = sys ->getSize();
    cout << "\nCalculo realizado!\n"<<endl;
    cout << "La solucion del sistema es:\n"<<endl;
    for(i=0;i<orden;i++){
        sol = sys -> getSolution(i);
        cout << "\t " << sol;
    }

}

short interfaz::final()
{
    string linea;
    int op=0;
    bool repite = true;
    fflush(stdin);

    do{
        cout<<"\n\n"<<endl;
        cout << "Que desea hacer?\n"<<endl;
        cout << "   1-Guardar resultados\n"<<endl;
        cout << "   2-Salir " << endl;
        cout << "\n   ";
        getline(cin, linea);

        repite=atoi(linea.c_str())>3||atoi(linea.c_str())<=0;
        if (repite)
        {
            cout<<"\n"<<endl;
            cout << "Ingrese un valor valido"<< endl;
            cout<<"\n"<<endl;
            system("pause");
            system("cls");
        }

    } while (!esNumero(linea)||repite);

    op = atoi(linea.c_str());
    system("cls");
    return op;
}

string interfaz::nomArchivoGuar(){
    string _nomArchivo;
    cout << "Nombre con el que desea guardar el archivo: " << endl;
     cout << "\n   ";
    cin >> (_nomArchivo);
    system("cls");
    return _nomArchivo;
}

string interfaz::nomArchivoCar(){
    string _nomArchivo;
    cout << "Nombre del archivo que desea cargar:\n " << endl;
    cout << "\t ";
    cin >> (_nomArchivo);
    system("cls");
    return _nomArchivo;
}

void interfaz:: mostrarMensaje(short error)
{
    if(error==20)
    cout << "\n\t No se pudo cargar el archivo \n\t" << endl;

    if(error==21)
    cout << "\n\t No se pudo guardar el archivo \n\t" << endl;

    if(error==22)
    cout << "\n\t Archivo guardado con exito \n\t" << endl;

    if(error==30)
    cout << "\n\t El sistema tiene infinitas soluciones \n\t" << endl;

    if(error==31)
    cout << "\n\t El sistema es inconsistente \n\t" << endl;

    if(error==99)
    {
        cout << "\n  Gracias por usar nuestro solver" << endl;
        cout << "\n  Desarrolladores:" << endl;
        cout << "\n\t      -Balaudo Tom\240s\n\t      -Bertolin Gianfranco\n\t      -D'angelo Pablo\n\t      -Loker Federico" << endl;
        cout << "\n\t TP1 Inofrm\240tica 2 UTN FRP\n" << endl;
    }
}







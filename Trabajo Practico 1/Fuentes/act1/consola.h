/**
 * @file consola.h
 * @author Bertolin Gianfranco (gianfrancobertolin@alu.frp.utn.edu.ar)
 * @brief Este header define la funcion que muestra un informe en la consola
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */
#ifndef CONSOLA_H
#define CONSOLA_H

/*!
 * \brief consola: Esta funcion muestra los titulos y detalles del informe
 *                 respecto de la integracion.
 */
void consola(double, double, int, int);
#endif // CONSOLA_H

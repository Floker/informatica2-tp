var searchData=
[
  ['selectcarga_36',['selectCarga',['../classinterfaz.html#acbdcb3422d611b057c4ac2e3acfe1572',1,'interfaz']]],
  ['selectmetodo_37',['selectMetodo',['../classinterfaz.html#a8ea425aaad105d03d2546a4df688a04b',1,'interfaz']]],
  ['setelement_38',['setElement',['../classeq_sys.html#a2e31dbb2a436baf3d1a49b90255c82bb',1,'eqSys']]],
  ['setsize_39',['setSize',['../classeq_sys.html#a2d558e62ccc11127f2556b68be5bc60a',1,'eqSys']]],
  ['setsolution_40',['setSolution',['../classeq_sys.html#aed3d29ae09eef7e65bdb436c323894a2',1,'eqSys']]],
  ['solutionarr_41',['solutionArr',['../classeq_sys.html#a2057083ec89f4200d278db45f72cc6bd',1,'eqSys']]],
  ['solve_42',['solve',['../class_cramer.html#a87937a1ca43edadc3ec7d86b1533a0fc',1,'Cramer::solve()'],['../class_gauss_el.html#a3afc620b9dafd7e3afaf71095018dbbe',1,'GaussEl::solve()']]],
  ['solver_43',['Solver',['../class_solver.html',1,'']]],
  ['solver_2eh_44',['solver.h',['../solver_8h.html',1,'']]],
  ['swap_5frow_45',['swap_row',['../class_gauss_el.html#a657836d9ba10c545be38fe4b5f394164',1,'GaussEl']]],
  ['sys_46',['sys',['../class_gauss_el.html#a2bc2d459dcba8e644fd583969ca3108d',1,'GaussEl']]]
];

/**
 * @file cramer.cpp
 * @author Balaudo Tomas (tomasbalaudo@alu.frp.utn.edu.ar)
 * @brief Este archivo implementa la resolución de sistemas de ecuaciones por método de Cramer.
 * @version 0.1
 * @date 2021-07-19
 * @copyright Copyright (c) 2021
 *
 */

#include <iostream>
#include "cramer.h"

Cramer::Cramer()
{

}

double Cramer::determinante3x3(double A [3][3])//funcion que permite obtener el determinante
{

    int n= 3;
    short int i,j,k; //para los ciclos
    double aux,pivote,pivote1,det=1;
    for(i=0; i<n; i++) //identificar el pivote
    {
        pivote=A[i][i];
        for(j=i+1; j<n; j++)//hacer reduccion a 0 de los elemento que están por debajo del pivote
        {
            pivote1=A[j][i];
            aux=pivote1/pivote;
            for (k=0;k<n;k++)
            A[j][k]=A[j][k]-aux* A[i][k];
        }
    }//realizado el codigo se obtiene la matriz triangular superior
    for(i=0; i<n; i++) //obtener el determinante
    det=det*A[i][i]; //recorre en diagonal y multiplia los elementos
    return det;
}

double Cramer::determinante2x2(double A[2][2])

{
    double det;
    det= (A[0][0]*A[1][1])+(A[0][1]*A[1][0]);
    return det;
}


short Cramer::solve(eqSys * sistema)
{
    short error = 0;
  if(sistema->getSize()==3)
      error = matriz3x3(sistema); //Áca está el tema
  else
      error = matriz2x2(sistema);
  return error;
}
short Cramer::matriz3x3(eqSys *sys)

{
    short error = 0;
       double d [3][3]= {
          { sys -> getElement(0,0), sys -> getElement(0,1), sys -> getElement(0,2) },
          { sys -> getElement(1,0), sys -> getElement(1,1), sys -> getElement(1,2) },   //Matriz del sistema
          { sys -> getElement(2,0), sys -> getElement(2,1), sys -> getElement(2,2) },
      };

       double d1[3][3]  = {
          { sys -> getElement(0,3), sys -> getElement(0,1), sys -> getElement(0,2) },
          { sys -> getElement(1,3), sys -> getElement(1,1), sys -> getElement(1,2) },   //Matriz con el vector de incognitas en la primer columna
          { sys -> getElement(2,3), sys -> getElement(2,1), sys -> getElement(2,2) },
      };

       double d2[3][3]  = {
          { sys -> getElement(0,0), sys -> getElement(0,3), sys -> getElement(0,2) },
          { sys -> getElement(1,0), sys -> getElement(1,3), sys -> getElement(1,2) },  //Matriz con el vector de incognitas en la segunda columna
          { sys -> getElement(2,0), sys -> getElement(2,3), sys -> getElement(2,2) },
      };

       double d3[3][3]  = {
          { sys -> getElement(0,0), sys -> getElement(0,1), sys -> getElement(0,3) },
          { sys -> getElement(1,0), sys -> getElement(1,1), sys -> getElement(1,3) },  //Matriz con el vector de incognitas en la tercer columna
          { sys -> getElement(2,0), sys -> getElement(2,1), sys -> getElement(2,3) },
      };



    double D = determinante3x3(d);
    double D1 = determinante3x3(d1);
    double D2 = determinante3x3(d2);  // Calculo de los determinantes mediante funcion
    double D3 = determinante3x3(d3);


    // Caso 1
    if (D != 0) {

        double x = D1 / D;
        double y = D2 / D;
        double z = D3 / D;
        sys->setSolution(0,x);
        sys->setSolution(1,y);
        sys->setSolution(2,z);

    }
    // Caso 2
    else
    {
            if (D1 == 0 && D2 == 0 && D3 == 0)
              {
                //Soluciones infintas
                error = 30;
              }
            else if (D1 != 0 || D2 != 0 || D3 != 0)
              {
               //No hay soluciones
                error = 31;
              }
    }
    return error;
}

short Cramer::matriz2x2(eqSys* sys)
{
    short error = 0;
    double d [2][2]= {
       { sys -> getElement(0,0), sys -> getElement(0,1) },
       { sys -> getElement(1,0), sys -> getElement(1,1) },

   };

    double d1 [2][2]= {
       { sys -> getElement(0,2), sys -> getElement(0,1) },
       { sys -> getElement(1,2), sys -> getElement(1,1) },

   };

    double d2 [2][2]= {
       { sys -> getElement(0,0), sys -> getElement(0,2) },
       { sys -> getElement(1,0), sys -> getElement(1,2) },

   };

    double D = determinante2x2(d);
    double D1 = determinante2x2(d1);
    double D2 = determinante2x2(d2);

    if (D != 0)
    {
       //Única solución
        double x = D1 / D;
        double y = D2 / D;
        sys->setSolution(0,x);
        sys->setSolution(1,y);
    }
    // Case 2
    else
      {
            if (D1 == 0 && D2 == 0)
            {
              //Soluciones infintas
              error = 30;
            }
            else if (D1 != 0 || D2 != 0 )
            {
              //No hay soluciones
              error = 31;
            }
      }
return error;

}

Cramer::~Cramer(){

}

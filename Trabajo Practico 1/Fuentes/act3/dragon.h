/**
 * @file dragon.h
 * @author Balaudo Tomas (tomasbalaudo@alu.frp.utn.edu.ar)
 * @brief Este header define la clase dragon que es una entidad del tablero
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */

#ifndef DRAGON_H
#define DRAGON_H

#include "jugador.h"

class Dragon
{
protected:
    /*!
     * \brief duenio: Jugador al que pertenece el dragon
     */
    Jugador * duenio;

    /*!
     * \brief posicion: Posicion del dragon en el tablero
     */
    short posicion;

    /*!
     * \brief incidencia: Incidencia del dragon en el juego.
     *                    1: Tiene incidencia.
     *                    0: No tiene incidencia.
     */
    bool incidencia;

public:
    /*!
     * \brief Dragon: Construye un objeto Dragon
     */
    Dragon();

    /*!
     * \brief posicionar: Setea la posicion inicial del dragon.
     * \param pos: Posicion inicial del dragon.
     */
    void posicionar(short pos);

    /*!
     * \brief desplazar: Desplaza el dragon en el tablero.
     * \return Posicion del dragon en el tablero.
     */
    short desplazar();

    /*!
     * \brief getPos: Devuelve la posicion del dragon en el tablero.
     * \return Posicion del dragon en el tablero.
     */
    short getPos();

    /*!
     * \brief setInc: Carga la incidencia del dragon en el juego.
     * \param inc:
     *              1: Tiene incidencia.
     *              0: No tiene incidencia.
     */
    void setInc(bool inc);

    /*!
     * \brief getInc: Incidencia del dragon en el juego.
     * \return
     *           1: Tiene incidencia.
     *           0: No tiene incidencia.
     */
    bool getInc();

    /*!
     * \brief setDuenio: Setea el jugador que es duenio del dragon.
     * \param due: Jugador al que el dragon pertenece.
     */
    void setDuenio(Jugador * due);

    /*!
     * \brief getDuenio: Devuelve el jugador que es duenio del dragon.
     * \return Jugador al que el dragon pertenece
     */
    Jugador *getDuenio();

};

#endif // DRAGON_H

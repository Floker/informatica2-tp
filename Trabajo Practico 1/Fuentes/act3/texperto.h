/**
 * @file texperto.h
 * @author Loker Federico (federicoloker@alu.frp.utn.edu.ar)
 * @brief Este header define la clase de tablero experto que es un modo de juego
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */

#ifndef TEXPERTO_H
#define TEXPERTO_H

#include "tablero.h"
#include "dragon.h"
#include "calabozo.h"

#define N_DRAGONES 2
#define N_CALABOZOS 3

class tExperto : public Tablero
{
protected:

    /*!
     * \brief dragonesJ1: Dragones del jugador 1
     */
    Dragon dragonesJ1[N_DRAGONES];

    /*!
     * \brief dragonesJ2: Dragones del jugador 2
     */
    Dragon dragonesJ2[N_DRAGONES];

    /*!
     * \brief calabozos: Calabozos del tablero
     */
    Calabozo calabozos[N_CALABOZOS];

public:
    /*!
     * \brief tExperto: Construye un tablero de nivel experto
     */
    tExperto();

    /*!
     * \brief inicializar: inicializa el tablero
     * \param j1: Jugador 1
     * \param j2: Jugador 2
     */
    void inicializar(Jugador *j1, Jugador *j2);

    /*!
     * \brief aplicarReglas: Aplica las reglas correspondientes del tablero
     * \return Retorna el codigo de evento
     */
    short aplicarReglas();

    /*!
     * \brief jugarCiclo: Juega un ciclo del juego (es decir, tirar dados y avanzar)
     * \return Retorna el codigo de evento
     */
    short jugarCiclo();

    /*!
     * \brief ~tExperto: Destruye un tablero de nivel experto
     */
    ~tExperto();
};

#endif // TEXPERTO_H

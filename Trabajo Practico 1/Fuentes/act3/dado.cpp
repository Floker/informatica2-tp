/**
 * @file dado.cpp
 * @author Balaudo Tomas (tomasbalaudo@alu.frp.utn.edu.ar)
 * @brief Este archivo implementa la clase dado que es una entidad del tablero
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */
#include "dado.h"

Dado::Dado()
{
  srand(time(NULL));
}


short Dado::tirarDado()
{
    short num;
    num = 1 + rand() % (6);
    return num;
}

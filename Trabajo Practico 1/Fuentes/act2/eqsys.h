/**
 * @file eqsys.h
 * @author Bertolin Gianfranco (gianfrancobertolin@alu.frp.utn.edu.ar)
 * @brief Este header define la clase que modela un sistema de ecuaciones
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */

#ifndef EQSYS_H
#define EQSYS_H


class eqSys
{
protected:
    /*!
     * \brief nUnknowns: Number of unknows of the system
     */
    unsigned int nUnknowns;

    /*!
     * \brief Coefmatrix: Dynamic matrix that modelates the system
     */
    double ** coefMatrix;

    /*!
     * \brief solutionArr: Dinamic array that contains the solution values
     */
    double *  solutionArr;

    /*!
     * \brief deleteMatrix: Deletes the dynamically allocated data
     * \return Zero for success
     */
    bool cleanDynamicData();

public:
    /*!
     * \brief eqSys: Construct an eqSys object
     */
    eqSys();

    /*!
     * \brief setSize: Sets the size of the matrix for a given number of unknowns
     * \param n: Number of unknows
     * \return Zero for success
     */
    bool setSize(unsigned int n);

    /*!
     * \brief getSize: Returns number of unknows
     * \return number of unknows
     */
    unsigned int getSize();

    /*!
     * \brief setElement allocates a given value to a given position
     * \param m: Row
     * \param n: Column
     * \param value: Value to allocate
     */
    void setElement(int m, int n, double value);

    /*!
     * \brief getElement returns the value at the given position
     * \param m: Rows
     * \param n: Colums
     * \return allocated value
     */
    double getElement(int m, int n);

    /*!
     * \brief setSolution: Allocates a given value on the solutions array
     * \param m: row
     * \param value: value to allocates
     */
    void setSolution(int m, double value);

    /*!
     * \brief getElement returns the value at the given position
     * \param m: Rows
     * \return allocated value
     */
    double getSolution(int m);

    /*!
     * \brief ~eqSys: Destruct an eqSys object
     */
    ~eqSys();

};

#endif // EQSYS_H

TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        calabozo.cpp \
        dado.cpp \
        dragon.cpp \
        interfaz.cpp \
        jugador.cpp \
        main.cpp \
        tablero.cpp \
        texperto.cpp \
        tintermedio.cpp

HEADERS += \
    calabozo.h \
    dado.h \
    dragon.h \
    interfaz.h \
    jugador.h \
    tablero.h \
    texperto.h \
    tintermedio.h

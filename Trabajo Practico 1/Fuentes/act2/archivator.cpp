/**
 * @file archivator.cpp
 * @author Loker Federico (federicoloker@alu.frp.utn.edu.ar)
 * @brief Este archivo implementa la clase archivator
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */

#include "archivator.h"

#include <iostream>

archivator::archivator()
{

}

short archivator::readFile(eqSys *sys, string path)
{

    FILE *fp;
    char buff[200]={0};
    string rawData;
    short errorCode = 0;

    unsigned int i = 0;
    unsigned int j = 0;
    unsigned int n = 0;

    // acondiciono el path para fopen
    string aux = "ejemplos/";
    aux += path;
    aux += ".json"; //concateno el tipo de archivo
    char filename [aux.length()+1];
    strncpy(filename, aux.c_str(),aux.length()+1);

    fp = fopen(filename, "r");
    // confirmo que el archivo existe o esta cargado
    if (fp == NULL) {
        errorCode = 20;
    }
    else{
        // leo el archivo
        while(fgets(buff, 200,fp))
        {
            //busco donde terminan los espacios del principio de cada linea
            for(j = 0; buff[j] == ' '; j++)
            {
            }
            //busco donde esta el final de la linea
            for(i = 0; buff[i] != '\n' && buff[i] != '}'; i++)
            {
            }
            //Si salí del for anterior porque era llave, sumo uno para incluirla
            // esto es porque el archivo puede no terminar con salto de linea
            if(buff[i]=='}') i++;
            //cargo el texto entre j e i en un string auxiliar
            for(unsigned int g = j; g<i; g++)
            {
                rawData.insert(rawData.length(), 1, buff[g]);
            }
        }

        // preparo el string a arr para parsear
        char arr[rawData.length() + 1];
        strncpy(arr, rawData.c_str(),rawData.length() + 1);

        //parseo el arreglo en un documento json
        rapidjson::Document d;
        d.Parse(arr);

        rapidjson::Value& s = d["size"];
        n = s.GetInt();
        sys->setSize(n);

        s = d["coefMatrix"];
        // cargo los valores a la matriz
        for(i=0; i<n;i++)
            for(j=0;j<n+1;j++)
                sys->setElement(i,j,s[i][j].GetDouble());

        fclose(fp);// cierro el archivo
    }
    return errorCode;
}

short archivator::writeFile(eqSys *sys, string path)
{
    FILE *fp;
    string buff;
    string aux = "true";
    unsigned int n = sys->getSize();
    short errorCode = 22;

    if(sys->getSolution(0) != 0)
        aux = "false";

    buff = "{\n";
    buff += "    \"size\": ";
    buff += to_string(n);
    buff += ",\n    ";
    buff += "\"coefMatrix\": [";
    for (unsigned int i = 0; i < n; i++)
    {
        buff += "\n        [";
        for (unsigned int j=0; j < n+1; j++)
        {
            buff += to_string(sys->getElement(i,j)) + ",";
        }
        buff = buff.substr(0, buff.size()-1);
        buff += "],";
    }
    buff = buff.substr(0, buff.size()-1);
    buff += "\n    ],";
    buff += "\n    \"isSolved\": ";
    buff += aux;
    buff += ",";
    buff += "\n    \"solution\": [";
    for (unsigned int i = 0; i<n; i++)
    {
        buff += to_string(sys->getSolution(i)) + ",";
    }
    buff = buff.substr(0, buff.size()-1);
    buff += "]\n}";

    // acondiciono el path para fopen
    string aux2 = "ejemplos/";
    aux2 += path;
    aux2 += ".json"; //concateno el tipo de archivo
    char filename [aux2.length()+1];
    strncpy(filename, aux2.c_str(),aux2.length()+1);

    fp = fopen(filename, "w");
    // confirmo que el archivo existe o esta cargado
    if (fp == NULL) {
        errorCode = 21;
    }
    else{
        char aux [buff.length() +1];
        strncpy(aux, buff.c_str(),buff.length() + 1);
        fprintf(fp, "%s", aux);
        fclose(fp); // cierro el archivo
    }

    return errorCode;
}

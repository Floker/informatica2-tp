/**
 * @file main.cpp
 * @author Loker Federico (fedelokeralu.frp.utn.edu.ar)
 * @brief Este es el archivo principal de la aplicacion
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */

#include <iostream>
#include <stdlib.h>
#include <conio.h>

#include "tablero.h"
#include "tintermedio.h"
#include "texperto.h"
#include "interfaz.h"
#include "windows.h"

using namespace std;

int main()
{
    Tablero * tab = nullptr;
    Interfaz ui;

    Jugador jugador1;
    Jugador jvirtual;

    short evento = 0;
    char aux = 0;
    bool okey = false;


    aux = ui.Welcome();
    if (aux == 'S' || aux == 's')
    {
        ui.clear();
        return 0;
    }

    string name = ui.setName();
    jugador1.setNom(name);

    do
    {
        aux = ui.Menu();

        // Polimorfismo al tablero
        if (aux == '1')
        {
            tab = new Tablero;
            tab->inicializar(&jugador1,&jvirtual);
            okey = true;
        }
        if (aux == '2')
        {
            tab = new tIntermedio;
            tab->inicializar(&jugador1,&jvirtual);
            okey = true;
        }
        if (aux == '3')
        {
            tab = new tExperto;
            tab->inicializar(&jugador1,&jvirtual);
            okey = true;
        }
        if (aux == 'S' || aux == 's')
        {
            ui.clear();
            return 0;
        }

    }while(!okey);

    bool seguir;
    do{
    evento = tab->jugarCiclo();
//    cout<< endl << "evento: " << evento << endl << endl;
    seguir = ui.showMessage(evento);
    }while(seguir);
    //cout << "Hello World!" << endl;

    delete(tab);
    return 0;
}


/**
 * @file archivator.h
 * @author Balaudo Tomas (tomasbalaudo@alu.frp.utn.edu.ar)
 * @brief Este header define la clase archivator.
 * @version 0.1
 * @date 2021-07-19
 *
 * @copyright Copyright (c) 2021
 *
 */


#ifndef ARCHIVATOR_H
#define ARCHIVATOR_H
#include "cliente.h"
#include <bits/stdc++.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include<stdlib.h>
using namespace std;

class archivator
{
public:

    /*!
     * \brief archivator: Crea un objeto de la clase archivator
     */
    archivator();


    /*!
    * \brief readFile: Lee un archivo .log y reeleva la infomarción que tiene en un vector de clientes
    * \param vec: Vector de clientes a ser llenados
    * \return int: 0 Ejecución existosa
    *              1 No existe el archivo
    *              2 No se pudo abrir el archivo
    */
   int readFile(std::vector<cliente *> &vec, string name);


   /*!
    * \brief writeFile: Genera un archivo .txt a partir del vector de clientes dado
    * \param vec: Vector de clientes a escribir
    * \return int: 0 Ejecución existosa
    *              1 No existe el archivo
    *              2 No se pudo abrir el archivo
    */
   int writeFile(std::vector<cliente *> &vec, string name);

};

#endif // ARCHIVATOR_H

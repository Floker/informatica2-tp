/**
 * @file solver.h
 * @author Loker Federico (federicoloker@alu.frp.utn.edu.ar)
 * @brief Este header define la clase abstracta Solver.
 * @version 0.1
 * @date 2021-07-19
 *
 * @copyright Copyright (c) 2021
 *
 */
#ifndef SOLVER_H
#define SOLVER_H
#include "eqsys.h"

class Solver
{
public:
    Solver();
    virtual short solve(eqSys *psys)=0;
    virtual ~Solver();
};

#endif // SOLVER_H

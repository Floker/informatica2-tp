TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        archivator.cpp \
        cramer.cpp \
        eqsys.cpp \
        gaussel.cpp \
        interfaz.cpp \
        main.cpp \
        solver.cpp

HEADERS += \
    archivator.h \
    cramer.h \
    eqsys.h \
    gaussel.h \
    interfaz.h \
    solver.h

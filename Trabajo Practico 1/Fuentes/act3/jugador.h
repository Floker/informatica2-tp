/**
 * @file jugador.h
 * @author Loker Federico (federicoloker@alu.frp.utn.edu.ar)
 * @brief Este header define la clase jugador que es una entidad del juego
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */

#ifndef JUGADOR_H
#define JUGADOR_H

#include <string>

using namespace std;

class Jugador
{
protected:

    /*!
     * \brief posicion: Posicion del jugador en el tablero
     */
    short posicion;

    /*!
     * \brief nombre: Nombre del jugador
     */
    string nombre;

    /*!
     * \brief habilitado: Habilitacion para tirar dados
     */
    bool habilitado;

public:
    /*!
     * \brief Jugador: Construye un objeto jugador.
     */
    Jugador();

    /*!
     * \brief setPos: Setea la posicion del jugador
     * \param pos: Posicion del jugador en el ta blero
     */
    void setPos(short pos);

    /*!
     * \brief getPos: Devuelve la posicion del jugador en el tablero.
     * \return Posicion del jugador.
     */
    short getPos();

    /*!
     * \brief setNom: Setea el nombre del jugador
     * \param nom: Nombre del jugador
     */
    void setNom(string nom);

    /*!
     * \brief getNom: Devuelve el nombre del jugador
     * \return Nombre del jugador
     */
    string getNom();

    /*!
     * \brief setHab: Setea si el jugador esta habilitado para tirar dados
     * \param hab: 1 para si, 0 para no
     */
    void setHab(bool hab);

    /*!
     * \brief getHab: devuelve si el jugador esta habilitado para tirar dados
     * \return 1 para si, 0 para no
     */
    bool getHab();

};

#endif // JUGADOR_H

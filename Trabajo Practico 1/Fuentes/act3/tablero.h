/**
 * @file tablero.h
 * @author Loker Federico (federicoloker@alu.frp.utn.edu.ar)
 * @brief Este header define la clase tablero
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */

#ifndef TABLERO_H
#define TABLERO_H

#include "jugador.h"
#include "dado.h"
#include <time.h>

class Tablero
{
protected:

    /*!
     * \brief jugador1: Jugador real
     */
    Jugador *jugador1;

    /*!
     * \brief jugador2: Jugador virtual
     */
    Jugador *jugador2;

    /*!
     * \brief dado: Dado del tablero
     */
    Dado dado;

    /*!
     * \brief getAleatorio: Devuelve un aleatorio para posicionar los elementos en el tablero
     * \return valor aleatorio del 3 al 49
     */
    short getAleatorio();

    /*!
     * \brief hayGanador: Dice si hay un ganador
     * \return Codigo de evento de ganador
     */
    short hayGanador();

    /*!
     * \brief aplicarReglas: Aplica las reglas correspondientes del tablero
     * \return Retorna el codigo de evento
     */
    virtual short aplicarReglas();

public:
    /*!
     * \brief Tablero: Cosntruye un tablero elemental
     */
    Tablero();

    /*!
     * \brief inicializar: inicializa el tablero
     * \param j1: Jugador 1
     * \param j2: Jugador 2
     */
    virtual void inicializar(Jugador *j1, Jugador *j2);

    /*!
     * \brief jugarCiclo: Juega un ciclo del juego (es decir, tirar dados y avanzar)
     * \return Retorna el codigo de evento
     */
    virtual short jugarCiclo();

    /*!
     * \brief ~Tablero: Destruye un tablero elemental
     */
    virtual ~Tablero();

};

#endif // TABLERO_H

/**
 * @file dado.h
 * @author Balaudo Tomas (tomasbalaudo@alu.frp.utn.edu.ar)
 * @brief Este header define la clase dado que es un objeto del tablero
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */

#ifndef DADO_H
#define DADO_H
#include <stdlib.h>
#include <time.h>
#include<iostream>
using namespace std;

class Dado
{
public:
    /*!
     * \brief Dado: Construye un objeto Dado.
     */
    Dado();

    /*!
    * \brief tirarDado: Simula que un jugador lanzó un dado.
    * \return Valor del dado
    */
   short tirarDado();

};

#endif // DADO_H

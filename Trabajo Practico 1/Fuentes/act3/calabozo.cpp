/**
 * @file calabozo.cpp
 * @author Loker Federico (federicoloker@alu.frp.utn.edu.ar)
 * @brief Este archivo implementa la clase calabozo que es una entidad del tablero
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */

#include "calabozo.h"

Calabozo::Calabozo()
{
    incidencia = 1;
}

void Calabozo::posicionar(short pos)
{
    posicion = pos;
}

short Calabozo::getPos()
{
    return posicion;
}

void Calabozo::setInc(bool inc)
{
    incidencia = inc;
}

bool Calabozo::getInc()
{
    return incidencia;
}

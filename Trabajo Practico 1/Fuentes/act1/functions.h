/**
 * @file functions.h
 * @author Bertolin Gianfranco (gianfrancobertolin@alu.frp.utn.edu.ar)
 * @brief Este header define la funcion a evaluar y la regla de Simpson 1/3
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */
#ifndef FUNCTIONS_H
#define FUNCTIONS_H
/*!
 * \brief f: Esta es la funcion dada a estudiar.
 * \param x: Este parametro, como en la integracion manual, va a ser la variable de integracion.
 * \return: La funcion va a retornar la imagen de la funcion en el punto dado por
 *          el parametro x.
 */
double f(double x);

/*!
 * \brief simpsons: Esta funcion aplica la regla de simpson 1/3.
 * \param f: En este paramtro se le pasa la direcicon de memoria de la funcion a integrar,
 *           definida en la funcion previa.
 * \param a: Extremo inferior de integracion.
 * \param b: Extremo superior de integracion.
 * \param n: Cantidad de intervalos dentro de los extemos de integracion.
 * \return: Retorna un double, que representaria el area bajo la curva dada por la funcion f
 *          y calculada con la regla de simpson 1/3.
 */
double simpsons(double f(double x),double a,double b,int n);

#endif // FUNCTIONS_H

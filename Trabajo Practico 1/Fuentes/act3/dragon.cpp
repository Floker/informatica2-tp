/**
 * @file dragon.cpp
 * @author Balaudo Tomas (tomasbalaudo@alu.frp.utn.edu.ar)
 * @brief Este archivo implementa la clase dragon que es una entidad del tablero
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */

#include "dragon.h"

Dragon::Dragon()
{
    incidencia = 1;
}

void Dragon::posicionar(short pos)
{
    posicion = pos;
}

short Dragon::desplazar()
{
    posicion++;
    return posicion;
}

short Dragon::getPos()
{
    return posicion;
}

void Dragon::setInc(bool inc)
{
    incidencia = inc;
}

bool Dragon::getInc()
{
    return incidencia;
}

void Dragon::setDuenio(Jugador * due)
{
    duenio = due;
}

Jugador * Dragon::getDuenio()
{
    return duenio;
}



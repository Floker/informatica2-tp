/**
 * @file archivator.cpp
 * @author Balaudo Tomas (tomasbalaudo@alu.frp.utn.edu.ar)
 * @brief Este archivo implementa la clase archivator
 * @version 0.1
 * @date 2021-11-15
 *
 * @copyright MIT Licence 2021
 *
 */


#include "archivator.h"

archivator::archivator()
{

}

int archivator::readFile(std::vector<cliente *> &vec, string name)
{
    string fecha;
    string duracion;
    string IP;
    string peticion;
    string tamanio;
    string respuesta;
    string URL;
    string linea, cont;
    ifstream log;

    log.open(name, ios::in);

    if(log.fail())
    {
      return 1; //No se pudo abrir
    }

    while(!log.eof())
    {
       getline(log,linea);
        //Busco fecha y borro la subcadena
       fecha= linea.substr(0, linea.find(' '));
       linea.erase(0,(linea.find(' ')+1));
       linea.erase(0,(linea.find_first_not_of(' ')));

       //Busco tiempo de conexión y borro la subcadena
       duracion= linea.substr(0,linea.find(' '));
       linea.erase(0,(linea.find(' ')+1));

       // Busco IP y borro la subcadena que la contiene
       IP= linea.substr(0,linea.find(' '));
       linea.erase(0,(linea.find(' ')+1));

       // Busco protocolo y borro la subcadena que la contiene
       peticion= linea.substr(0,linea.find(' '));
       linea.erase(0,(linea.find(' ')+1));

      // Busco cantidad de datos y borro la subcadena que la contiene
       tamanio= linea.substr(0,linea.find(' '));
       linea.erase(0,(linea.find(' ')+1));

       // Busco respuesta y borro la subcadena que la contiene
       respuesta= linea.substr(0,linea.find(' '));
       linea.erase(0,(linea.find(' ')+1));

        // Busco URl y borro la subcadena que la contiene
       URL= linea.substr(0,linea.find(' '));
       linea.erase(0,(linea.find(' ')+1));

       cliente * usuario;
       usuario= new cliente(fecha, duracion, IP, peticion, respuesta, tamanio, URL);
       vec.push_back(usuario);

        // cout << fecha << tconect << IP <<protocolo<<cdata<<respuesta<<URL << endl;
    }
     log.close();
    return 0;
}

int archivator::writeFile(std::vector<cliente *> &vec, string name)
{
    int tamaniosum=0;
    string aux, aux2;
    ofstream reg;
    reg.open(name);


    if(!reg.is_open())
    {
        return 1; //Error al abrir el archivo
    }


    for (auto elem = vec.begin(); elem != vec.end(); elem++)
    {
        tamaniosum+= (*elem)->getTamanio();
        aux= string((*elem)->getData());
        reg << aux << endl;
    }

    aux2= to_string(tamaniosum);

    aux="Cantidad de bytes transmitidos: "+ aux2;
    reg << aux << endl;

    return 0;
}

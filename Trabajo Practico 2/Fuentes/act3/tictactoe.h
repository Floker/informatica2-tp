/**
 * @file tictactoe.h
 * @author Loker Federico (federicoloker@alu.frp.utn.edu.ar)
 * @brief Este header define la clase tictactoe
 * @version 0.1
 * @date 2021-11-10
 *
 * @copyright MIT Licence 2021
 *
 */


#ifndef TICTACTOE_H
#define TICTACTOE_H

#include <QDialog>
#include <qextserialport.h>
#include <QMessageBox>

#include "tablero.h"

QT_BEGIN_NAMESPACE
namespace Ui { class tictactoe; }
QT_END_NAMESPACE


/*!
 * \brief La clase tictactoe modela la vista y controlador del juego tictactoe.
 */
class tictactoe : public QDialog
{
    Q_OBJECT

public:

    /*!
     * \brief tictactoe: construye un objeto de la clase tictactoe.
     * \param parent: Puntero al padre de clase QWidget.
     */
    tictactoe(QWidget *parent = nullptr);


    /*!
     * \brief Destruye el objeto tictactoe.
     * 
     */
    ~tictactoe();

    QextSerialPort * puertoSerie;
    
    Ui::tictactoe *ui;
public slots:

    /*!
     * \brief recibirDatos: Método callback para la recepcion de datos vía puerto serial.
     * 
     */
    void recibirDatos();


    /*!
     * \brief enviarDatos: Método para la transmisión de datos vía puerto serial.
     */
    void enviarDatos();

protected slots:
    /*!
     * \brief on_btnConectar_clicked: Método llamado mediante la UI al clickear el boton conectar.
     */
    void on_btnConectar_clicked();

    /*!
     * \brief on_btn_Nombre_clicked: Método llamado mediante la UI al clickear el boton de guardar nombre.
     */
    void on_btn_Nombre_clicked();

    /*!
     * \brief on_btn_00_clicked: Método llamado mediante la UI al clickear la casilla 00 del tablero.
     */
    void on_btn_00_clicked();

    /*!
     * \brief on_btn_01_clicked: Método llamado mediante la UI al clickear la casilla 01 del tablero.
     */
    void on_btn_01_clicked();

    /*!
     * \brief on_btn_02_clicked: Método llamado mediante la UI al clickear la casilla 02 del tablero.
     */
    void on_btn_02_clicked();

    /*!
     * \brief on_btn_10_clicked: Método llamado mediante la UI al clickear la casilla 10 del tablero.
     */
    void on_btn_10_clicked();

    /*!
     * \brief on_btn_11_clicked: Método llamado mediante la UI al clickear la casilla 11 del tablero.
     */
    void on_btn_11_clicked();

    /*!
     * \brief on_btn_12_clicked: Método llamado mediante la UI al clickear la casilla 12 del tablero.
     */
    void on_btn_12_clicked();

    /*!
     * \brief on_btn_20_clicked: Método llamado mediante la UI al clickear la casilla 20 del tablero.
     */
    void on_btn_20_clicked();

    /*!
     * \brief on_btn_21_clicked: Método llamado mediante la UI al clickear la casilla 21 del tablero.
     */
    void on_btn_21_clicked();

    /*!
     * \brief on_btn_22_clicked: Método llamado mediante la UI al clickear la casilla 22 del tablero.
     */
    void on_btn_22_clicked();

protected:

    /*!
     * \brief tab: Objeto tablero que contiene el estado del juego.
     */
    Tablero tab;

    /*!
     * \brief refreshMat: Método que actualiza la UI del tablero.
     */
    void refreshMat();

    /*!
     * \brief played: Método llamado al pulsar un casillero del tablero
     * \param i: Posición del casillero llamado.
     * \param j: Posición del casillero llamado.
     */
    void played(int i,int j);

    /*!
     * \brief arrbtn: Vector que contiene todos los nueve botones del tablero.
     */
    std::vector<QPushButton*> arrbtn;

    /*!
     * \brief showWinner: Método que informa quién fue el ganador de la partida.
     * \param pwin: Código del jugador ganador.
     */
    void showWinner(char pwin);

};
#endif // TICTACTOE_H

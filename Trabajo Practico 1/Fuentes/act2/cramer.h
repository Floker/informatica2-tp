/**
 * @file cramer.h
 * @author Balaudo Tomas (tomasbalaudo@alu.frp.utn.edu.ar)
 * @brief Este header implementa la resolución de sistemas de ecuaciones por método de Cramer.
 * @version 0.1
 * @date 2021-07-19
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef CRAMER_H
#define CRAMER_H

#include "solver.h"

class Cramer : public Solver
{
    protected:

        /*!
         * \brief determinante3x3: Calcula el determinante de una matrix 3x3
         * \param A: Matrix a la cual se le calcula el determinante
         * \return Valor del determinante
         */
        double determinante3x3(double A [3][3]);

        /*!
         * \brief determinante2x2: Calcula el determinante de una matrix 2x2
         * \param A: Matrix a la cual se le calcula el determinante
         * \return Valor del determinante
         */
        double determinante2x2(double A[2][2]);

        /*!
         * \brief matriz3x3: Resuelve mediante Cramer para una matrix de 3x3
         * \param sys: Sistema de ecuaciones a ser resuelto
         */
        short matriz3x3(eqSys *sys);

        /*!
         * \brief matriz2x2:  Resuelve mediante Cramer para una matrix de 2x2
         * \param sys: Sistema de ecuaciones a ser resuelto
         */
        short matriz2x2(eqSys *sys);

    public:

        /*!
         * \brief Cramer: Construye un objeto Cramer
         */
        Cramer();

        /*!
         * \brief solve: Resuelve el sistema dado mediante el metodo de cramer
         * \param psys:  Sistema de ecuaciones a ser resuelto
         * \return Cero para ejecucion satisfactoria, ver codigo de errores documentacion
         */
        short solve(eqSys * psys);
        /*!
         * \brief ~Cramer: Destruye un objeto Cramer
         */
        ~Cramer();
};

#endif // CRAMER_H

/**
 * @file cliente.h
 * @author Balaudo Tomas (tomasbalaudo@alu.frp.utn.edu.ar)
 * @brief Este header define la clase cliente.
 * @version 0.1
 * @date 2021-07-19
 *
 * @copyright Copyright (c) 2021
 *
 */


#ifndef CLIENTE_H
#define CLIENTE_H
#include <bits/stdc++.h>
#include <string.h>
#include <iostream>
#include<stdlib.h>
using namespace std;

class cliente
{
protected:

    /*!
     * \brief fecha: Fecha en la que se realizó la conexión
     */
    string fecha;

    /*!
     * \brief duracion: duración de la conexión
     */
    string duracion;

    /*!
     * \brief IP: IP desde la cual se hizo la consulta
     */
    string IP;

    /*!
     * \brief peticion: Petición del usuario al serivdor
     */
    string peticion;

    /*!
     * \brief respuesta: Respuesta del servidor al usuario
     */
    string respuesta;

    /*!
     * \brief tamanio: Tamaño de la petición realizada en bytes
     */
    string tamanio;
    /*!
     * \brief URL: URL del contenido solicitado
     */
    string URL;
public:
    /*!
     * \brief cliente: Crea un objeto de la clase cliente
     * \param pfecha: Fecha en la que se realizó la conexión
     * \param pduracion: Duración de la conexión
     * \param pIP: IP desde la cual se hizo la consulta
     * \param ppeticion: Petición del usuario al serivdor
     * \param prespuesta: Respuesta del servidor al usuario
     * \param ptamanio: Tamaño de la petición realizada en bytes
     * \param pURL: URL del contenido solicitado
     */
    cliente(string pfecha,string pduracion, string pIP, string ppeticion,string prespuesta, string ptamanio, string pURL);


    /*!
     * \brief getData: Genera un string con información reelevante
     * \return std::string : Información reelevante
     */
    std::string getData();


    /*!
     * \brief getTamanio: Retorna la cantidad de bytes transmitidos al cliente
     * \return int: Cantidad de bytes transmitidos
     */
    int getTamanio();
};

#endif // CLIENTE_H

/**
 * @file tablero.h
 * @author Loker Federico (federicoloker@alu.frp.utn.edu.ar)
 * @brief Este header define la clase tablero
 * @version 0.1
 * @date 2021-11-10
 *
 * @copyright MIT Licence 2021
 *
 */


#ifndef TABLERO_H
#define TABLERO_H

#include <string>
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include <qbytearray.h>
#include <QString>
/*!
 * \brief La clase Tablero modela y aporta logica a un tablero de juego de tictactoe.
 */
class Tablero
{
protected:

    /*!
     * \brief matrix: Contiene las celdas del tablero y a que jugador pertenecen.
     */
    char matrix[3][3];


    /*!
     * \brief player1: Nombre del Jugador 1.
     */
    QString player1;


    /*!
     * \brief player2: Nombre del jugador 2.
     */
    QString player2;


    /*!
     * \brief winner: Codigo del jugador ganador. '1' para jugador 1, '2' para jugador 2.
     */
    char winner;


    /*!
     * \brief turno: Codigo del jugador que debe jugar. '1' para jugador 1, '2' para jugador 2.
     */
    char turno;


    /*!
     * \brief areEqual: Indica si 3 casillas tienen piezas del mismo jugador.
     * \param i1: Posicion de primera casilla a evaluar.
     * \param j1: Posicion de primera casilla a evaluar.
     * \param i2: Posicion de segunda casilla a evaluar.
     * \param j2: Posicion de segunda casilla a evaluar.
     * \param i3: Posicion de tercera casilla a evaluar.
     * \param j3: Posicion de tercera casilla a evaluar.
     * \return bool:    true:  Las 3 casillas son iguales.
     *                  false: No cumplen.
     */
    bool areEqual(int i1, int j1,
                  int i2, int j2,
                  int i3, int j3);

public:

    /*!
     * \brief Tablero: Construye un objeto tablero
     */
    Tablero();


    /*!
     * \brief reset: Vuelve el estado del juego al inicial o comienza una partida nueva.
     * \param all:
     *              True: Borra toda la informacion de la partida.
     *              False: Borra solo la matriz y el ganador, conservando nombre de jugadores y turno.
     */
    void reset(bool all);


    /*!
     * \brief setState: Actualiza el estado del juego mediante una cadena de caracteres de entrada.
     * \param rawData: Informacion en formato JSON.
     */
    void setState(QByteArray rawData);


    /*!
     * \brief setWinner: Cambia el estado del atributo winner.
     * \param win: Nuevo estado a tomar por el atributo.
     */
    void setWinner(char win);


    /*!
     * \brief getWinner: Informa el estado actual del atributo winner.
     * \return char:    '1' para jugador 1 ganador
     *                  '2' para jugador 2 ganador
     *                  '0' para juego en curso.
     */
    char getWinner();


    /*!
     * \brief getState: Genera una cadena de caracteres en formato JSON para informar el nuevo estado del juego.
     * \return QByteString: Estado del juego en formato JSON.
     */
    QByteArray getState(void);


    /*!
     * \brief setPlayer1Name: Actualiza el nombre de jugador 1 al valor dado.
     * \param pname: Nuevo nombre del jugador 1.
     */
    void setPlayer1Name(QString pname);


    /*!
     * \brief setPlayer2Name: Actualiza el nombre de jugador 2 al valor dado.
     * \param pname: Nuevo nombre del jugador 2.
     */
    void setPlayer2Name(QString pname);


    /*!
     * \brief getPlayer1Name: Informa el nombre de jugador 1.
     * \return QString: Nombre del jugador.
     */
    QString getPlayer1Name(void);


    /*!
     * \brief getPlayer2Name: Informa el nombre de jugador 2.
     * \return QString: Nombre del jugador.
     */
    QString getPlayer2Name(void);


    /*!
     * \brief getTurn: Informa quien es el proximo jugador habilitado.
     * \return char:    '1' para jugador 1.
     *                  '2' para jugador 2.
     */
    char getTurn();


    /*!
     * \brief setTurn: Actualiza quien es el proximo jugador habilitado a partir del parametro dado.
     * \param pturno:   '1' para jugador 1.
     *                  '2' para jugador 2.
     */
    void setTurn(char pturno);


    /*!
     * \brief checkWin: Informa si hay un jugador ganador.
     * \return char:    '1' para jugador 1 ganador
     *                  '2' para jugador 2 ganador
     *                  '0' para juego en curso.
     */
    char checkWin();


    /*!
     * \brief setMatrix: Actualiza una casilla de la matriz con un valor dado
     * \param i:         Posicion de la casilla.
     * \param j:         Posicion de la casilla.
     * \param val:       Nuevo valor a insertar.
     */
    void setMatrix(int i, int j, int val);

    /*!
     * \brief getMatrix: Devuelve el valor contenido por la matriz en cierta posicion dada.
     * \param i:         Posicion de la casilla.
     * \param j:         Posicion de la casilla.
     * \return int       Valor ASCII del caracter contenido en esa casilla.
     */
    int getMatrix(int i, int j);


};

#endif // TABLERO_H

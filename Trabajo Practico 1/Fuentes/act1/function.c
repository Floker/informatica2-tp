/**
 * @file function.c
 * @author Bertolin Gianfranco (gianfrancobertolin@alu.frp.utn.edu.ar)
 * @brief Este archivo implementa las funciones de la libreria funcions.h
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */
#include <math.h>


double f(double x){
    double e = exp(1.0);
    return pow(e,(-3*x/2))*pow(x,6)*M_PI*sin(3*x);
}


double simpsons(double f(double x),double a,double b,int n){
  double h,integral,x,sum=0;
  int i;

  h=fabs(b-a)/n;        // Calculo de "h"

  for(i=1;i<n;i++){     // Esta iteracion evalúa la funcion en "n" intervalos.
    x = a+i*h;          // Aca se define en que punto se evaluará la funcion.

    if(i%2==0){         // Para los intervalos pares, el resultado de la evaluacion se multiplica por 2,
      sum=sum + 2*f(x);
    }
    else{
      sum=sum + 4*f(x);   // y para los impares se multiplica por 4.
    }
  }

  /*    ejemplo de lo anterior, si tenemos un intervalo [0;8],las evaluaciones en 2,4 y 6
   *    se multiplicaran por 2, y las evaluaciones en 1,3,5 y 7 se multiplicaran por 4.
   *    LOS EXTREMOS SE EVALUAN LUEGO.
  */

  integral=(h/3)*(f(a)+f(b)+sum);     /* Una vez acumuladas las evaluaciones, se suman  con las evaluaciones
                                      de la funcion en los extremos (a)y(b) y todo se multiplica por h/3 */
    return integral;
}

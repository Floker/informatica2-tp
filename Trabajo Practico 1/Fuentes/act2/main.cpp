/**
 * @file main.cpp
 * @author Loker Federico (fedelokeralu.frp.utn.edu.ar)
 * @brief Este es el archivo principal de la aplicacion
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */

#include <iostream>
#include "gaussel.h"
#include "eqsys.h"
#include "cramer.h"
#include "archivator.h"
#include "interfaz.h"

using namespace std;


int main()
{
    eqSys ecuacion;
    interfaz ui;
    archivator archi;
    string fileName;
    Solver *Sol = NULL;
    short metodoCarga;
    short errorCode;
    short metodoResolucion;
    short selectMenu;



    // Selecciono metodo de carga del sistema
    metodoCarga = ui.selectCarga();

    if (metodoCarga == 1) // leo archivo
    {
        do{
            fileName = ui.nomArchivoCar();
            errorCode = archi.readFile(&ecuacion,fileName);
            ui.mostrarMensaje(errorCode);
        }while(errorCode);
    }
    else if (metodoCarga == 2) // cargo por consola
    {
        ui.cargarSys(&ecuacion);
    }

    // Selecciono metodo de resolucion
    metodoResolucion = ui.selectMetodo(ecuacion.getSize());

    if (metodoResolucion == 1) //gauss
    {
        Sol = new GaussEl(); //polimorfismo
        errorCode = Sol->solve(&ecuacion);
    }
    else if (metodoResolucion == 2) //cramer
    {
        Sol = new Cramer(); //polimorfismo
        errorCode = Sol->solve(&ecuacion);
    }
    if(!errorCode)
    {
        ui.mostrarSol(&ecuacion);
    }
    else
    {
        ui.mostrarMensaje(errorCode);
    }

    // Selecciono menu final
    do {
        selectMenu = ui.final();
        if(selectMenu == 1)
        {
            do{
                fileName = ui.nomArchivoGuar();
                errorCode = archi.writeFile(&ecuacion,fileName);
                ui.mostrarMensaje(errorCode);
            }while(errorCode != 22);
        }
    } while (selectMenu != 2);

    ui.mostrarMensaje(99);

    delete Sol;
    return 0;

}

/**
 * @file tictactoe.cpp
 * @author Loker Federico (federicoloker@alu.frp.utn.edu.ar)
 * @brief Este archivo implementa la clase tictactoe
 * @version 0.1
 * @date 2021-11-10
 *
 * @copyright MIT Licence 2021
 *
 */


#include "tictactoe.h"
#include "ui_tictactoe.h"

#include "portconf.h"
#include "ui_portconf.h"


#include <qextserialport.h>
#include <QextSerialEnumerator.h>

int whoami = 0;

void tictactoe::enviarDatos(){

    QByteArray payload = tab.getState();

    if(puertoSerie->isOpen())
        {
            puertoSerie->write(payload);
        }
}

void tictactoe::recibirDatos(){
    QByteArray payload = "";
    while(puertoSerie->bytesAvailable())
    {
        payload = puertoSerie->readAll();
    }
    if(payload != "")
        tab.setState(payload);

    refreshMat();
}


tictactoe::tictactoe(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::tictactoe)
{
    ui->setupUi(this);
    puertoSerie = new QextSerialPort();
    puertoSerie->setBaudRate(BAUD9600);
    puertoSerie->setDataBits(DATA_8);
    puertoSerie->setFlowControl(FLOW_OFF);
    puertoSerie->setParity(PAR_NONE);
    puertoSerie->setStopBits(STOP_2);

    connect(puertoSerie,
            SIGNAL(readyRead()),
            this,
            SLOT(recibirDatos()));

    ui->btn_Nombre->blockSignals(true);
    ui->gridLayout->blockSignals(true);

    arrbtn.push_back(ui->btn_00);
    arrbtn.push_back(ui->btn_01);
    arrbtn.push_back(ui->btn_02);
    arrbtn.push_back(ui->btn_10);
    arrbtn.push_back(ui->btn_11);
    arrbtn.push_back(ui->btn_12);
    arrbtn.push_back(ui->btn_20);
    arrbtn.push_back(ui->btn_21);
    arrbtn.push_back(ui->btn_22);

    ui->labTurno->setText("Desconectado");
}

tictactoe::~tictactoe()
{
    disconnect(puertoSerie, SIGNAL(readyRead()),
               this, SLOT(recibirDatos()));
    if (puertoSerie->isOpen())
        puertoSerie->close();
    delete puertoSerie;
    delete ui;
}


void tictactoe::on_btnConectar_clicked()
{
    portconf cfg(this);
    if (puertoSerie->isOpen())
        puertoSerie->close();
    QList<QextPortInfo> puertos =
            QextSerialEnumerator::getPorts();
    foreach( QextPortInfo unPuerto, puertos )
    {
        if(unPuerto.portName != "")
        cfg.ui->cmbPuertos->addItem(unPuerto.portName);
    }
    if(cfg.exec())
    {
       puertoSerie->setPortName("\\\\.\\" + cfg.ui->cmbPuertos->currentText());
       puertoSerie->open(QIODevice::ReadWrite);
    }
    if(puertoSerie->isOpen()){
      ui->btn_Nombre->blockSignals(false);
      ui->labTurno->setText("Ingrese su nombre");
    }
    else
      ui->labTurno->setText("Desconectado");
}

void tictactoe::on_btn_Nombre_clicked()
{
    QString nombre1 = ui->leJugador->text();

    if(tab.getPlayer1Name() == "")
    {
        tab.setPlayer1Name(nombre1);
        whoami = 1;
        tab.setTurn('2');
    }else
    {
        tab.setPlayer2Name(nombre1);
        whoami = 2;
        tab.setTurn('1');
    }

    enviarDatos();

    ui->btn_Nombre->blockSignals(true);
}



void tictactoe::on_btn_00_clicked()
{
    played(0,0);
}

void tictactoe::on_btn_01_clicked()
{
    played(0,1);
}

void tictactoe::on_btn_02_clicked()
{
    played(0,2);
}

void tictactoe::on_btn_10_clicked()
{
    played(1,0);
}

void tictactoe::on_btn_11_clicked()
{
    played(1,1);
}

void tictactoe::on_btn_12_clicked()
{
    played(1,2);
}

void tictactoe::on_btn_20_clicked()
{
    played(2,0);
}

void tictactoe::on_btn_21_clicked()
{
    played(2,1);
}

void tictactoe::on_btn_22_clicked()
{
    played(2,2);
}

void tictactoe::refreshMat()
{
    int ii = 0;
    int jj = 0;

    for (auto elem = arrbtn.begin(); elem != arrbtn.end(); elem++)
    {
        if(tab.getMatrix(jj,ii) == '1')
            (*elem)->setText("X");
        else
        if(tab.getMatrix(jj,ii) == '2')
            (*elem)->setText("O");
        if(tab.getMatrix(jj,ii) == '0')
            (*elem)->setText("-");
        if(ii == 2)
        {
            jj++;
            ii = 0;
        }
        else
            ii++;
    }



    if(tab.getTurn() == '1' && tab.getPlayer1Name() != "")
    {
        QString texto = "Juega: ";
        texto += tab.getPlayer1Name();
        ui->labTurno->setText(texto);
    }
    if(tab.getTurn() == '2' && tab.getPlayer2Name() != "")
    {
        QString texto = "Juega: ";
        texto += tab.getPlayer2Name();
        ui->labTurno->setText(texto);
    }
    char winner = tab.checkWin();
    tab.setWinner(winner);
    showWinner(winner);
}

void tictactoe::played(int i, int j)
{
    if((tab.getTurn() == (char)whoami+48)){
        if(whoami == 2)
        {
            tab.setTurn('1');
        }
        else
        {
            tab.setTurn('2');
        }
        tab.setMatrix(i,j,whoami);
        enviarDatos();
        refreshMat();
    }
}

void tictactoe::showWinner(char pwin)
{
    QString ganador = "";
    if(pwin != '0'){
        if(pwin == '1')
            ganador += tab.getPlayer1Name();
        if(pwin == '2')
            ganador += tab.getPlayer2Name();

        ganador += " ha ganado el juego";
        QMessageBox aviso;
        aviso.setWindowTitle("Juego terminado");
        aviso.setText(ganador);
        aviso.exec();
        tab.reset(0);
        refreshMat();
        ui->btn_Nombre->blockSignals(false);
    }
}

/**
 * @file tablero.cpp
 * @author Balaudo Tomas (tomasbalaudo@alu.frp.utn.edu.ar)
 * @brief Este archivo implementa la clase de tablero intermedio que es un modo de juego
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */

#include "tintermedio.h"

tIntermedio::tIntermedio()
{

}

void tIntermedio::inicializar(Jugador *j1, Jugador *j2)
{
    jugador1 = j1;
    jugador2 = j2;
    srand(time(NULL));
    jugador1->setPos(1);
    jugador1->setHab(1);
    jugador2->setPos(1);
    jugador2->setHab(1);

    for(short i = 0; i<2; i++)
    {
        dragonesJ1[i].setDuenio(jugador1);
        dragonesJ1[i].posicionar(getAleatorio());
        dragonesJ1[i].setInc(1);

        dragonesJ2[i].setDuenio(jugador2);
        dragonesJ2[i].posicionar(getAleatorio());
        dragonesJ2[i].setInc(1);
    }

}

short tIntermedio::aplicarReglas()
{
   short aux=0;

      //Jugador 1
       if(dragonesJ1[0].getPos() == jugador1->getPos()||dragonesJ1[1].getPos() == jugador1->getPos())
       {
              jugador1->setPos(jugador1->getPos()+5); // Avanza 5 posiciones si se encuentra con uno de sus dragones
              aux = 151;
       }

       if (dragonesJ2[0].getPos() == jugador1->getPos()||dragonesJ2[1].getPos() == jugador1->getPos())
       {
           jugador1->setPos(jugador1->getPos()-5); // Retrocede 5 posiciones si se encuentra con uno de los dragones rivales
           aux = 150;
       }

       //Jugador 2
       if(dragonesJ2[0].getPos() == jugador2->getPos()||dragonesJ2[1].getPos() == jugador2->getPos())
       {
              jugador2->setPos(jugador2->getPos()+5); // Avanza 5 posiciones si se encuentra con uno de sus dragones
              aux = 251;
       }

       if (dragonesJ1[0].getPos() == jugador2->getPos()||dragonesJ1[1].getPos() == jugador2->getPos())
       {
           jugador2->setPos(jugador2->getPos()-5); // Retrocede 5 posiciones si se encuentra con uno de los dragones rivales
           aux = 250;
       }

       if(!aux)
           aux = hayGanador();

       return aux;
}

short tIntermedio::jugarCiclo()
{
    short j1pos = jugador1->getPos() + dado.tirarDado();
    short j2pos = jugador2->getPos() + dado.tirarDado();

    jugador1->setPos(j1pos);
    jugador2->setPos(j2pos);

    short evento = aplicarReglas();


    for (int i = 0; i < N_DRAGONES; i++)
    {
        dragonesJ1[i].desplazar();
        dragonesJ2[i].desplazar();
    }

    return evento;
}

tIntermedio::~tIntermedio(){

}

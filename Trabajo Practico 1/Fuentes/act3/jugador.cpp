/**
 * @file jugador.cpp
 * @author Loker Federico (federicoloker@alu.frp.utn.edu.ar)
 * @brief Este archivo implementa la clase jugador que es una entidad del juego
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */

#include "jugador.h"

Jugador::Jugador()
{
    habilitado = 1;
}

void Jugador::setPos(short pos)
{
    if(pos < 0)
        posicion = 0;
    else
        posicion = pos;
}

short Jugador::getPos()
{
    return posicion;
}

void Jugador::setNom(string nom)
{
    nombre = nom;
}

string Jugador::getNom()
{
    return nombre;
}

void Jugador::setHab(bool hab)
{
    habilitado = hab;
}

bool Jugador::getHab()
{
    return habilitado;
}


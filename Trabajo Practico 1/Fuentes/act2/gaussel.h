/**
 * @file gaussel.h
 * @author D'angelo Pablo (pablodangelomejias@alu.frp.utn.edu.ar)
 * @brief Este header define la clase del metodo de resolucion por Eliminacion de Gauss
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */

#ifndef GAUSSEL_H
#define GAUSSEL_H

#include "solver.h"
#include <iostream>
#include <iomanip>
#include <math.h>
#include <stdlib.h>
#include "eqsys.h"

class GaussEl : public Solver
{
protected:
    /*!
     * \brief sys: pointer of the equation system to be solved
     */
    eqSys * sys;

    /*!
     * \brief mat: Auxiliary matrix to be handle
     */
    double ** mat;

    /*!
     * \brief nUnknowns: Number of unknows of the system
     */
    unsigned int nUnknowns;

    /*!
     * \brief forwardElim: Reduce matrix to Row Echelon Form (r.e.f.)
     * \return Value to indicate whether matrix is singular or not (-1)
     */
    int forwardElim();

    /*!
     * \brief backSub: Calculates the unknowns' values
     */
    void backSub();

    /*!
     * \brief swap_row: Swaps two rows
     * \param i: Row to be swapped
     * \param j: Row to be swapped
     */
    void swap_row(int i, int j);

    /*!
     * \brief Deletes the dynamically allocated data
     * \return Zero for success
     */
    bool cleanDynamicData();

public:

    /*!
     * \brief GaussEl: Construct a GaussEl object
     */
    GaussEl();

    /*!
     * \brief solve: Solves an equation system
     * \param psys: (referenced) Equation system to be solved
     */
    short solve(eqSys * psys);

    /*!
     * \brief ~GaussEl: Destruct a GaussEl object
     */
    ~GaussEl();
};


#endif // GAUSSEL_H

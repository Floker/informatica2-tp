/**
 * @file cliente.cpp
 * @author Balaudo Tomas (tomasbalaudo@alu.frp.utn.edu.ar)
 * @brief Este archivo implementa la clase cliente
 * @version 0.1
 * @date 2021-11-15
 *
 * @copyright MIT Licence 2021
 *
 */


#include "cliente.h"


cliente::cliente(string pfecha,string pduracion, string pIP, string ppeticion,string prespuesta, string ptamanio, string pURL)
{
  fecha=pfecha;
  duracion=pduracion;
  IP= pIP;
  peticion= ppeticion;
  respuesta=prespuesta;
  tamanio= ptamanio;
  URL=pURL;

}

std::string cliente::getData()
{
   std::string aux="";

   aux+= IP+ "\t" +  tamanio;

   return aux;
}

int cliente::getTamanio()
{
    return stoi(tamanio);
}

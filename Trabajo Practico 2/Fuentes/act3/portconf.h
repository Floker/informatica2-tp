/**
 * @file portconf.h
 * @author Loker Federico (federicoloker@alu.frp.utn.edu.ar)
 * @brief Este header define la clase portconf
 * @version 0.1
 * @date 2021-11-10
 *
 * @copyright MIT Licence 2021
 *
 */


#ifndef PORTCONF_H
#define PORTCONF_H

#include <QDialog>

namespace Ui {
class portconf;
}

/*!
 * \brief La clase portconf modela la UI de la conexión por puerto serial.
 */
class portconf : public QDialog
{
    Q_OBJECT
    
public:
    explicit portconf(QWidget *parent = 0);
    ~portconf();
    Ui::portconf *ui;

};

#endif // PORTCFG_H

/**
 * @file gaussel.cpp
 * @author D'angelo Pablo (pablodangelomejias@alu.frp.utn.edu.ar)
 * @brief Este archivo implementa la clase del metodo de resolucion por Eliminacion de Gauss
 * @version 0.1
 * @date 2021-07-30
 * @copyright MIT Licence 2021
 *
 */

#include "gaussel.h"

using namespace std;

GaussEl::GaussEl()
{

}

int GaussEl::forwardElim()
{
    for (unsigned int k=0; k<nUnknowns; k++)
    {
        // Inicializar el valor máximo y el índice para pivote
        unsigned int i_max = k;
        int v_max = mat[i_max][k];

        /* encuentra una mayor amplitud para el pivote si lo hay */
        for (unsigned int i = k+1; i < nUnknowns; i++)
            if (abs(mat[i][k]) > v_max)
                v_max = mat[i][k], i_max = i;

        /* si un elemento diagonal principal es cero,
         * denota que la matriz es singular, y
         * conducirá a una división por cero más adelante. */
        if (!mat[k][i_max])
            return k; // La matriz es singular

        /* Intercambiar la fila de mayor valor con la fila actual */
        if (i_max != k)
            swap_row(k, i_max);


        for (unsigned int i=k+1; i<nUnknowns; i++)
        {
            /* factor f para establecer el k-ésimo elemento de la fila actual en 0,
             * y luego la k-ésima columna restante a 0 */
            double f = mat[i][k]/mat[k][k];

            /* restar el quinto múltiplo del k-ésimo correspondiente
                elemento de fila  */
            for (unsigned int j=k+1; j<=nUnknowns; j++)
                mat[i][j] -= mat[k][j]*f;

            /* relleno de la matriz triangular inferior con ceros  */
            mat[i][k] = 0;
        }

    }
    return -1;
}

void GaussEl::backSub()
{
    double x[nUnknowns];  // An array to store solution

    /* Empiezo a calcular desde la última ecuación hasta la
        primera */
    for (int i = (nUnknowns-1) ; i >= 0; i--)
    {
        x[i] = mat[i][nUnknowns];

        /* Inicializo j a i + 1 ya que la matriz es triangular superior */
        for (unsigned int j=i+1; j<nUnknowns; j++)
        {
            /* restar todos los valores de RHS excepto el coeficiente
             *  de la variable cuyo valor se está calculando  */
            x[i] -= mat[i][j]*x[j];
        }

        /* dividir el RHS por el coeficiente
         * de la incógnita que se está calculando */
        x[i] = x[i]/mat[i][i];
    }

    for (unsigned int i=0; i<nUnknowns; i++)
    {
        sys->setSolution(i,x[i]);
    }
}

void GaussEl::swap_row(int i, int j)
{

    for (unsigned int k=0; k<nUnknowns+1; k++)
    {
        double temp = mat[i][k];
        mat[i][k] = mat[j][k];
        mat[j][k] = temp;
    }
}

bool GaussEl::cleanDynamicData()
{
    for (unsigned int i = 0; i < nUnknowns; ++i)
        delete [] mat[i];
    delete [] mat;

    return true;
}

short GaussEl::solve(eqSys *psys)
{

    int error = 0;
    sys = psys;

    nUnknowns = sys->getSize();
    // copia de la matriz
    mat  = new double *[nUnknowns];
    for(unsigned int i = 0; i<nUnknowns; i++)
    {
        mat[i] = new double [nUnknowns+1];
        for(unsigned int j = 0; j<(nUnknowns+1); j++)
            mat[i][j] = sys->getElement(i,j);
    }


    /* reducción a  r.e.f. */
    int singular_flag = forwardElim();

    /* Si la matriz es singular */
    if (singular_flag != -1)
    {
        /* si el RHS de la ecuación correspondiente a la fila cero es 0,
         * el sistema tiene infinitas soluciones,
         * de lo contrario es inconsistente */
        if (mat[singular_flag][nUnknowns])
            error = 31;
        else
            error = 30;
        return error;
    }

    /* obtener una solución para el sistema
     * mediante sustitución hacia atrás */
    backSub();

    return 0;

}

GaussEl::~GaussEl()
{
    cleanDynamicData();
}









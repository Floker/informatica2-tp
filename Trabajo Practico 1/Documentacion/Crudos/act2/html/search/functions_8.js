var searchData=
[
  ['selectcarga_88',['selectCarga',['../classinterfaz.html#acbdcb3422d611b057c4ac2e3acfe1572',1,'interfaz']]],
  ['selectmetodo_89',['selectMetodo',['../classinterfaz.html#a8ea425aaad105d03d2546a4df688a04b',1,'interfaz']]],
  ['setelement_90',['setElement',['../classeq_sys.html#a2e31dbb2a436baf3d1a49b90255c82bb',1,'eqSys']]],
  ['setsize_91',['setSize',['../classeq_sys.html#a2d558e62ccc11127f2556b68be5bc60a',1,'eqSys']]],
  ['setsolution_92',['setSolution',['../classeq_sys.html#aed3d29ae09eef7e65bdb436c323894a2',1,'eqSys']]],
  ['solve_93',['solve',['../class_cramer.html#a87937a1ca43edadc3ec7d86b1533a0fc',1,'Cramer::solve()'],['../class_gauss_el.html#a3afc620b9dafd7e3afaf71095018dbbe',1,'GaussEl::solve()']]],
  ['swap_5frow_94',['swap_row',['../class_gauss_el.html#a657836d9ba10c545be38fe4b5f394164',1,'GaussEl']]]
];

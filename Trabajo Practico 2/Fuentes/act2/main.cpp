/**
 * @file main.cpp
 * @author Balaudo Tomas (tomasbalaudo@alu.frp.utn.edu.ar) 
 * @brief Este archivo implementa el archivo principal de la aplicacion
 * @version 0.1
 * @date 2021-11-10
 *
 * @copyright MIT Licence 2021
 *
 */


#include <iostream>
#include <fstream>
#include<stdlib.h>
#include <string.h>
#include <bits/stdc++.h>
#include "archivator.h"
#include "cliente.h"
using namespace std;

int main()
{

    string nameIn="access.log";
    string nameOut="salida.txt";

    std::vector<cliente *> clientvec;
    archivator archi;
    int state;
   state= archi.readFile(clientvec, nameIn);
   if(state ==1)
   {
      cout<<"No existe el archivo .log"<<endl; //No existe el archivo
   }

       else
       cout<<"Se abrio correctamente el archivo .log"<<endl;

   state= archi.writeFile(clientvec, nameOut);
   if(state ==1)
   {
      cout<<"No existe el archivo .log"<< endl; //No existe el archivo
   }

       else
      cout<<"Se abrio correctamente el archivo .txt"<<endl;


       //Borro la memoria dinamica

       for (auto elem = clientvec.begin(); elem != clientvec.end(); elem++)
       {
           delete(*elem);
       }
       clientvec.clear();

    return 0;
}

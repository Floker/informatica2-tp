/**
 * @file tablero.cpp
 * @author Loker Federico (federicoloker@alu.frp.utn.edu.ar)
 * @brief Este archivo implementa la clase tablero
 * @version 0.1
 * @date 2021-11-10
 *
 * @copyright MIT Licence 2021
 *
 */


#include "tablero.h"

/*
{"players":["",""],"matrix":[[0,0,0],[0,0,0],[0,0,0]],"winner":0,"turn":1}

*/


Tablero::Tablero()
{
    reset(1);
}

void Tablero::reset(bool all)
{
    if(all){
    player1 = "";
    player2 = "";
    winner = '0';
    turno = '0';
    }

    for (int i = 0; i<3; i++)
        for (int j = 0; j<3; j++)
            matrix[i][j]='0';
}

void Tablero::setState(QByteArray rawData)
{

    rapidjson::Document d;
    d.Parse(rawData);

    //leo nombre de jugadores
    rapidjson::Value& s = d["players"];
    player1 = s[0].GetString();
    player2 = s[1].GetString();

    //leo matriz
    s = d["matrix"];
    for (int i = 0; i<3; i++)
        for (int j = 0; j<3; j++)
            matrix[i][j] = (char)(s[i][j].GetInt()+48);

    // leo ganador
    s = d["winner"];
    winner = (char)(s.GetInt()+48);
    // leo turno
    s = d["turn"];
    turno = (char)(s.GetInt()+48);

}

void Tablero::setWinner(char win)
{
    winner = win;
}

char Tablero::getWinner()
{
    return winner;
}


QByteArray Tablero::getState()
{
    QByteArray data2 = "";
    std::string data;
    data = "{\"players\":[\"" + player1.toStdString() + "\",\"" + player2.toStdString() + "\"],";
    data += "\"matrix\":[";
    for (unsigned int i = 0; i < 3; i++)
    {
        data += "[";
        for (unsigned int j=0; j < 3; j++)
        {
            data += (char)matrix[i][j];
            data += ",";
        }
        data = data.substr(0, data.size()-1);
        data += "],";
    }
    data = data.substr(0, data.size()-1);
    data += "],";
    data += "\"winner\":";
    data += (char)winner;
    data += ",";
    data += "\"turn\":";
    data += (char)turno;
    data += "}";
    data2 = data2.fromStdString(data);
    return data2;
}

void Tablero::setPlayer1Name(QString pname)
{
    player1 = pname;
}

void Tablero::setPlayer2Name(QString pname)
{
    player2 = pname;
}

QString Tablero::getPlayer1Name()
{
    return player1;
}

QString Tablero::getPlayer2Name()
{
    return player2;
}

char Tablero::getTurn()
{
    return turno;
}

void Tablero::setTurn(char pturno)
{
    turno = pturno;
}

char Tablero::checkWin()
{
           // horizontales
            if (areEqual(0,0,
                         1,0,
                         2,0))
            return matrix[0][0];
            else
            if (areEqual(0,1,
                         1,1,
                         2,1))
            return matrix[0][1];
            else
            if (areEqual(0,2,
                         1,2,
                         2,2))
            return matrix[0][2];
            else
           // verticales
            if (areEqual(0,0,
                         0,1,
                         0,2))
            return matrix[0][0];
            else
            if (areEqual(1,0,
                         1,1,
                         1,2))
            return matrix[1][0];
            else
            if (areEqual(2,0,
                         2,1,
                         2,2))
            return matrix[2][0];
            else
           // cruzados
            if (areEqual(0,0,
                         1,1,
                         2,2))
            return matrix[0][0];
            else
            if (areEqual(2,0,
                         1,1,
                         0,2))
            return matrix[0][2];
            else
                return '0';
}


bool Tablero::areEqual(int i1, int j1,
                       int i2, int j2,
                       int i3, int j3)
{
    if(matrix[i1][j1] != '0' && matrix[i2][j2] != '0' && matrix[i3][j3] != '0')
    {
        if(matrix[i1][j1] == '1' && matrix[i2][j2] == '1' && matrix[i3][j3] == '1')
            return 1;
        else if(matrix[i1][j1] == '2' && matrix[i2][j2] == '2' && matrix[i3][j3] == '2')
            return 1;
        else
            return 0;
    }
    else
        return 0;

}

void Tablero::setMatrix(int i, int j, int val)
{
    if(val == 1 && matrix[i][j] =='0')
        matrix[i][j] = '1';
    else
    if(val == 2 && matrix[i][j] =='0')
        matrix[i][j] = '2';
}

int Tablero::getMatrix(int i, int j)
{
    return matrix[i][j];
}


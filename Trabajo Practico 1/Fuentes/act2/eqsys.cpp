/**
 * @file eqsys.cpp
 * @author Bertolin Gianfranco (gianfrancobertolin@alu.frp.utn.edu.ar)
 * @brief Este header define la clase que modela un sistema de ecuaciones
 * @version 0.1
 * @date 2021-07-30
 * @copyright MIT Licence 2021
 *
 */

#include "eqsys.h"

bool eqSys::cleanDynamicData()
{
    for (unsigned int i = 0; i < nUnknowns; ++i)
        delete [] coefMatrix[i];
    delete [] coefMatrix;

    return true;
}

eqSys::eqSys()
{

}

bool eqSys::setSize(unsigned int n)
{
    bool fail = true;
    if (n >= 2)
    {
    fail = false;
    nUnknowns = n;
    coefMatrix = new double *[nUnknowns];
    solutionArr = new double [nUnknowns];
    for (unsigned int i = 0; i < nUnknowns; ++i)
        coefMatrix[i] = new double[nUnknowns+1];
    }
    return fail;
}

unsigned int eqSys::getSize()
{
    return nUnknowns;
}

void eqSys::setElement(int m, int n, double value)
{
    coefMatrix[m][n] = value;
}

double eqSys::getElement(int m, int n)
{
    return coefMatrix[m][n];
}

void eqSys::setSolution(int m, double value)
{
    solutionArr[m] = value;
}

double eqSys::getSolution(int m)
{
    return solutionArr[m];
}

eqSys::~eqSys()
{
    cleanDynamicData();
}

/**
 * @file tintermedio.h
 * @author Loker Federico (federicoloker@alu.frp.utn.edu.ar)
 * @brief Este header define la clase de tablero intermedio que es un modo de juego
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */

#ifndef TINTERMEDIO_H
#define TINTERMEDIO_H

#include "tablero.h"
#include "dragon.h"

#define N_DRAGONES 2

class tIntermedio : public Tablero
{
protected:
    /*!
     * \brief dragonesJ1: Dragones del jugador 1
     */
    Dragon dragonesJ1[N_DRAGONES];
    /*!
     * \brief dragonesJ2: Dragones del jugador 2
     */
    Dragon dragonesJ2[N_DRAGONES];

public:

    /*!
     * \brief tIntermedio: Construye un tablero de nivel intermedio
     */
    tIntermedio();

    /*!
     * \brief inicializar: inicializa el tablero
     * \param j1: Jugador 1
     * \param j2: Jugador 2
     */
    void inicializar(Jugador *j1, Jugador *j2);

    /*!
     * \brief aplicarReglas: Aplica las reglas correspondientes del tablero
     * \return Retorna el codigo de evento
     */
    short aplicarReglas();

    /*!
     * \brief jugarCiclo: Juega un ciclo del juego (es decir, tirar dados y avanzar)
     * \return Retorna el codigo de evento
     */
    short jugarCiclo();

    /*!
     * \brief tIntermedio: Destruye un tablero de nivel intermedio
     */
    ~tIntermedio();
};

#endif // TINTERMEDIO_H

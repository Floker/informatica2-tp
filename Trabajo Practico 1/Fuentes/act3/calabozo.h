/**
 * @file calabozo.h
 * @author Loker Federico (federicoloker@alu.frp.utn.edu.ar)
 * @brief Este header define la clase calabozo que es una entidad del tablero
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */
#ifndef CALABOZO_H
#define CALABOZO_H


class Calabozo
{
protected:
    /*!
     * \brief posicion: Posicion del calabozo en el tablero.
     */
    short posicion;

    /*!
     * \brief incidencia: Incidencia del calabozo en el juego.
     *                    1: Tiene incidencia.
     *                    0: No tiene incidencia.
     */
    bool incidencia;

public:
    /*!
     * \brief Calabozo: Construye un objeto calabozo
     */
    Calabozo();

    /*!
     * \brief posicionar: Setea la posicion del calabozo en el tablero.
     * \param pos: Posicion del calabozo en el tablero.
     */
    void posicionar(short pos);

    /*!
     * \brief getPos: Devuelve la posicion del calabozo en el tablero
     * \return Posicion del calabozo en el tablero.
     */
    short getPos();

    /*!
     * \brief setInc: Define si un calabozo tiene incidencia en el juego o no.
     * \param inc:
     *              1: Tiene incidencia.
     *              0: No tiene incidencia.
     */
    void setInc(bool inc);

    /*!
     * \brief getInc: Incidencia del calabozo en el juego.
     * \return
     *           1: Tiene incidencia.
     *           0: No tiene incidencia.
     */
    bool getInc();
};

#endif // CALABOZO_H

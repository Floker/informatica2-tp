/**
 * @file interfaz.cpp
 * @author D'angelo Pablo (pablodangelomejias@alu.frp.utn.edu.ar)
 * @brief Este archivo implementa la clase de la interfaz de consola
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */

#include "interfaz.h"
#include <windows.h>
#include <iostream>
#include <conio.h>
#include <string>


//#define Color     //Opcional para que cambie de color el menu de vienvenida
/*
NEGRO            = 0  |  GRIS OBSCURO  = 8
AZUL             = 1  |  CELESTE       = 9
VERDE            = 2  |  VERDE CLARO   = A
CELESTE SATURADO = 3  |  CYAN CLARO    = B
ROJO             = 4  |  ROJO CLARO    = C
MAGENTA          = 5  |  MAGENTA CLARO = D
MARRON           = 6  |  AMARILLO      = E
GRIS CLARO       = 7  |  BLANCO        = F
*/
using namespace std;


Interfaz::Interfaz()
{

};


void Interfaz::Image(){
    system("cls");      //Limpia la pantalla
    system("mode con: cols=160 lines=52");      //Setea el tamaño de la consola
  //system("color 02");     //Cambia el color del texto y fondo del CMD

    cout <<"                                                                  `.-::-.              .-::-.`                                                                   \n";
    cout <<"                                                          .:+sydmNds/-`                  `-/sdNmdyo+:.                                                           \n";
    cout <<"                       `   `                        `:+ydNMMMMdo-`                            `-odMMMMNdy+:`                        `   `                        \n";
    cout <<"                      -- `+`                    -+ymMMMMMMMh+`                                    `+hMMMMMMMmy+-                     +` --                       \n";
    cout <<"                     /o -y`                 -+hNMMMMMMMNmo.`-:/ooyyhddmmdyo+/-`  `-:+oydmmddhyyoo/:-`.omNMMMMMMMNh+-                 `y- o/                      \n";
    cout <<"                   `ys`sd.              `/yNMMMMMmhssssyhmNMMMMMMMmho/-`                `-/ohmMMMMMMMNmhysssshmMMMMMNy/`              .ds`sy`                    \n";
    cout <<"                  /mo/my`            .+dMMMMmysosymNMMMMMMMMMMNy/.                            ./yNMMMMMMMMMMNmysosymMMMMd+.            `ym/om/                   \n";
    cout <<"                -hNymN+      :s- -./dMMMdysshmMMMMMMMMMMMMMMh:                                    :hMMMMMMMMMMMMMMmhssydMMMd/.- -s:      +NmyNh-                 \n";
    cout <<"             `:hMMNMNdyo/-.`  -NhyddhhssdNMMMMMMMMMMMMMMMMN/                                        /NMMMMMMMMMMMMMMMMNdsshhddyhN-  `.-/oydNMNMMh:`              \n";
    cout <<"           `+dhhdNNMMMMMds:.   sMM+NmmMMMMMMMMMMMMMMMMMMMM/                                          /MMMMMMMMMMMMMMMMMMMMmmM+MMs   .:sdMMMMMNNdhhd+             \n";
    cout <<"      -ydshNMmNMNNMMNNMMMMmy+:./MM+mMMMNmmdddddmNNMMMMMMMN.                                          .NMMMMMMMNNmdddddmmNMMMm+MM/.:+ymMMMMNNMMNNMNmMNhsdy-       \n";
    cout <<"      +NMNmhymhsyhdMMmdNMMMNyo-:MM/MMMMMMMMMMMNNmdhyysyhmMo                                          oMNhysyyhdmNNMMMMMMMMMMM/MM:-oyNMMMNdmMMdhyshmyhmNMN+       \n";
    cout <<"      `s:-`.:o+yNd/+dyyyMMMMm::oMd+MMMMMMMMMMMMMMMMMMMNds+/.                                        `/+sdNMMMMMMMMMMMMMMMMMMM+dMo::mMMMMyyyd+/dNy+o:.`-:s`       \n";
    cout <<"         -:-yNMy:` +/ssomMMMNN::MohMMMMMMMMMMMMMMMMMMMd+-`                                            `-+dMMMMMMMMMMMMMMMMMMMhoM:/NMMMMmoss/o `:yMNy-:-          \n";
    cout <<"          `:y++- -o+sNyNNMMMN+d:M:NMMMMMmNMMMMMMMMMMd-                                                    -dMMMMMMMMMMNmMMMMMN:M:d+NMMMNNyNs+o- -++y:`           \n";
    cout <<"              .//o/dMMMMMMMmm/-/N/MMMMMMMmhMMMMMMMMh`                                                      `hMMMMMMMMhmMMMMMMM/N/-/mmMMMMMMMd/o:/.               \n";
    cout <<"          .:++./ohMMMMMMMMMyo: +dyMMMMMMMMNyhMMMMMm`                                                        `mMMMMMhyNMMMMMMMMyd+ :oyMMMMMMMMMho/.++:.           \n";
    cout <<"      `:o+. `symMMMMMMMMddN..``doNMMMMMMMMMMd+dMMMs                                                          sMMMd+dMMMMMMMMMMNod``..NddMMMMMMMMmys` .+o:`       \n";
    cout <<"    .+/` /++dMMMMMMMMMMm-m:   osNMMMMMMMMMMMMMs+mMo                                                          oMm+sMMMMMMMMMMMMMNso   :m-mMMMMMMMMMMd++/ `/+.     \n";
    cout <<"   +s/.  /mMMMMMMMNo+Nh.:-   +yNMMMNdMMMMMMMMMMm/sy                                                          ys/mMMMMMMMMMMdNMMMNy+   -:.hN+oNMMMMMMMm/  ./s+    \n";
    cout <<"  s- `-/dMMMMMMMMMh .+     `sdMMMMMmyMMMMMMMMMMMMy:`                                                        `:yMMMMMMMMMMMMymMMMMMds`     +. hMMMMMMMMMd/-` -s   \n";
    cout <<" :+    +mNNNMMMMMMMy.    .oNMMMMMMMhsMMMMmho+:--..`                                                          `..--:+ohmMMMMshMMMMMMMNo.    .yMMMMMMMNNNN+    +:  \n";
    cout <<" osssyyyyyyyysssssyhddhhmMMMMMMMMMMyoMd+.                                                                              .+dMoyMMMMMMMMMMmdhddhysssssyyyyyyyyssso  \n";
    cout <<"  sssssssooooooossyyyssssshmMMMMMMMs//                                                                                    //sMMMMMMMmhsssssyyyssooooooosssssss   \n";
    cout <<"   ``..--::/+osyyyyyso+++oo+oo/--/s/                                                                                       /s/--/oo+oo++oosyyyyyso+/::--..``     \n";


}
short Interfaz::Welcome(){

    short opcion;
    system("cls");      //Limpia la pantalla
#ifdef Color
    for(int i=0; i<5; i++){

        system("color 01");
        Image();
        Sleep(150);

        system("color 02");
        Image();
        Sleep(150);

        system("color 04");
        Image();
        Sleep(150);

        system("color 05");
        Image();
        Sleep(150);

        system("color 06");
        Image();
        Sleep(150);

        system("color 03");
        Image();
        Sleep(150);
    }
#endif
    Image();
    system("color 02");
    gotoxy(64,24);      //Posiciona el cursor entre los dragones
    cout <<"BIENVENIDO A CALABOZOS Y DRAGONES";
    gotoxy(75,36);
    cout <<"1 - Jugar";
    gotoxy(75,38);
    cout <<"S - Salir";
    gotoxy(80,40);
    opcion = getch();
    gotoxy(2,50);       //Posiciona el cursor fuera de la imagen de los dragones

    return opcion;
}


short Interfaz::Menu(){

    //system("color 02");     //Cambia el color del texto y fondo del CMD
    short opcion;

    Image();        //Invoca la imagen de los dragones

    gotoxy(70,24);      //*Mueve el cursor para escribir entre los dragones*
    cout <<"SELECCIONE EL NIVEL";
    gotoxy(72, 28);     //*
    cout <<"1 - BASICO.";
    gotoxy(72, 32);     //*
    cout <<"2 - INTERMEDIO.";
    gotoxy(72, 36);     //*
    cout <<"3 - EXPERTO.";
    gotoxy(72, 40);     //*
    cout <<"S - SALIR.";
    gotoxy(78, 44);     //*
    opcion = getch();
    gotoxy(2,50);       //Posiciona el cursor fuera de la imagen de dragones

    return opcion;
}

void Interfaz::GameOver(short evento){
    system("cls");      //Limpia la pantalla
    system("mode con: cols=160 lines=52");      //Setea el tamaño de la consola
    if(evento==21){
    system("cls");
    system("color 0E");
    cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
    cout << "                                                                      Felicidades " << j1Nom <<"                         \n"
            "                                                                 Has luchado y has prevalecido                     \n";
    cout << "                                                               Mereces una recompensa digna de ti                  \n"
            "                                                                               ..                                  \n"
            "                                                                              oMNo                                 \n"
            "                                                                             yMmmMs                                \n"
            "                                                                        -///:Mh  dM::\\-                           \n"
            "                                                                       .+hNMMM+  +MMMNh+.                          \n"
            "                                                                           /mMy  yMm/                              \n"
            "                                                                      /syyo-`oMsyMo`-oyys/                         \n"
            "                                                     sdhhd/         .mMNhhmMh.oMMo.hMmhdNMm.         /dhhds        \n"
            "                                                    y+    o    ::   /Mm    :md.NN`mm-   .mM/   ::    o    +y       \n"
            "                                               +/.--h      yyosy+  :oNN/.   -M:dd:M-   ./NNo:  +hsoyy      h---/+  \n"
            "                                              .yo+sdMy.    yMMM-    +mhhNs  .N:mm:N   sNhhm+    -MMMs    .hMds+ss  \n"
            "                                                     :syyhNMs:/o./:/y       od-MM-do       y///.o::sMNhyys:        \n"
            "                                                        /hM+      yMh     .yd:mMMd-dy.     hMy      oMh/           \n"
            "                                                          -+      +M+soooss/.+/hh/+`:ssooos+M+     .+-             \n"
            "                                                           -o++osyohmso++++/-`ss`-/+++oosmh+ys+/+o-                \n"
            "                                                             o:/oso:/--::-::-://-:--:::::/:oso+:o                  \n"
            "                                                             yhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhs                  \n"
            "                                                           .NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN.                \n"
            "                                                            sdddddddddddddddddddddddddddddddddddds                 \n"
            "                                                             ohhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhs                  \n"
            "                                                                                                                   \n";


     }
    if(evento == 31){
    system("cls");
    system("color 02");
    cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
    cout <<"                                                                      .:/+ooo+/:--:/+ooo+/-                        \n"
           "                                                                     /+/---:--/o/+++----::++:                      \n"
           "                                                                    /+::+/:-:+/+- +//+-:++:/+/                     \n"
           "                                                                    oo://:/:++++o++/o+//`///++`                    \n"
           "                                                                    +++////+++++++/o++o/://:+o`                    \n"
           "                                                                    .+++++/+++++-`/++++//++/+/                     \n"
           "                                                                     .+++++--+o-  -++.-/+//+:                      \n"
           "                                                            .-::::-   /++/+/-/+/:+/./+//+/.    ....                \n"
           "                                                         :/++oooo++o// :++//+:++/+:+//++:`.://++++++//-            \n"
           "                                                       .+++++::::/+++/++ ++///+:-/+//+: :/+//+/+++//::++.          \n"
           "                                                       ++++/``+/                                   -+/:++          \n"
           "                                                       oo//: `++        Has tenido suerte          .//o++          \n"
           "                                                       :+/++//+/ Tu oponente ha caido en desgracia +++o+:          \n"
           "                                                        /+/++o+/   Toma tu recompensa y descansa   //+/+           \n"
           "                                                        .++/+++o                                   o//+/+:         \n"
           "                                                        ++./+:.+/+o/+.:++//+/.++.-++ ./+//+/-++++/+/:++`oo         \n"
           "                                                        ++.++:`++o++/+++/+/.  //:///  `-++//+oooo/o `+o.oo         \n"
           "                                                        .++:/+/:::++++++/.    //-://    `:+/:/+o+/::++//+-         \n"
           "                                                         `:++//+++//+/:`      ++/+/+      `:/+///++++++/.          \n"
           "                                                           `.::////:.`       o+-../          `-::::::.`            \n"
           "                                                                           o+++:..                                 \n"
           "                                                                         o++:`-o                                   \n"
           "                                                                        ++++:/+/                                   \n"
           "                                                                     //:.  .-.                                     \n";


    }
    ///31 gana j1 porque el 2 se cayo. 32 viceversa
    if(evento==22){
    system("color 04");
#ifdef Color
        for(int i=0; i<15; i++){

            system("cls");
            if(i%2 == 0){

                system("color 04");

            }else{
                system("color 07");

            }
#endif
   cout << "\n\n\n\n\n\n\n\n\n";
   cout << "                                                                      ./oyhdmmNNNNNNmdhs+:                  \n"
           "                                                                   /ymMMMMMMMMMMMMMMMMMMMMMms/.             \n"
           "                                                                :yNMMMMMMMMMMMMMMMMMMMMMMMMMMMMd+.          \n"
           "                                                              /dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNh:        \n"
           "                                                            /mMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh:      \n"
           "                                                          -hMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMs     \n"
           "                                                         +NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMd.   \n"
           "                                                         MMMMMMM                                  MMMMMMd   \n"
           "                                                        oMMMMMM  Mi mas sentido pesame " << j1Nom <<"      MMMMMMs  \n"
           "                                                        yMMMMMM           No has llegado           MMMMMMm  \n"
           "                                                        hMMMMMMm                                  mMMMMMMm  \n"
           "                                                        dMMMmmNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNmmMMMd  \n"
           "                                                        hMMM:.-/ydNMMMMMMMMMMMMMMMMMMMMMMMMMMMMmho:...NMMs  \n"
           "                                                        sMMMo     -/ydNMMMMMMMMMMMMMMMMMMMMMms:.     .MMM:  \n"
           "                                                        :MMMN-       .-+ymMMMMMMMMMMMMMMNds:.        yMMm   \n"
           "                                                         dMMMd-          .:ohmMMMMMMMmy+-          .sMMM+   \n"
           "                                                         -dMMMNhs+/:-.    .-:sNMMMMMh+-.    ..-:/ohmMMMm    \n"
           "                                                          .sNMMMMMMNNmddddmNMMMmhMmMMMNmddddmmNMMMMMMMMo    \n"
           "                                                           -sMMMMMMMMMMMMMMMMMm sM yMMMMMMMMMMMMMMMMMMM-    \n"
           "                                                          .NMMMMMMMMMMMMMMMMMm  NM  sMMMMMMMMMMMMMMMMMM:    \n"
           "                                                          :MMMMMMMMMmdysMMMMMNomMMN/hMMMMMMMmhssyNMMMMMo    \n"
           "                                                          .NMMMMMNy/.   mMMMMMMMMMMMMMMMMMMm.    sMMMNo     \n"
           "                                                           `:dmhds.    dMMMMMMMMMMMMNNMMMMo      omdo-      \n"
           "                                                                        hMMMNMMMMdMMM/yMMMN.                \n"
           "                                                                        /MMMh MMm.MMM:MMMMs                 \n"
           "                                                                        .MMM  MMh MMM MMMN                  \n"
           "                                                                         NMM  MMh MMN MMMh                  \n"
           "                                                                         mMM  MMh MMm MMMy                  \n"
           "                                                                         dMM  MMh MMm MMMy                  \n"
           "                                                                         hMM  MMh MMd MMMs                  \n"
           "                                                                         yMM  MMh MMd MMM+                  \n"
           "                                                                         oMM  MMh MMy MMM-                  \n"
           "                                                                         :MM  MMh MMo MMm                   \n"
           "                                                                          :s: NNy md. dy-                   \n";
        Sleep(200);
#ifdef Color
        }
#endif
    }
    if (evento == 32){
    system("color 04");

#ifdef Color
        for(int i=0; i<15; i++){

            system("cls");
            if(i%2 == 0){

                system("color 04");

            }else{
                system("color 07");

            }
#endif
   cout << "\n\n\n\n\n\n\n\n\n";
   cout << "                                                                      ./oyhdmmNNNNNNmdhs+:                  \n"
           "                                                                   /ymMMMMMMMMMMMMMMMMMMMMMms/.             \n"
           "                                                                :yNMMMMMMMMMMMMMMMMMMMMMMMMMMMMd+.          \n"
           "                                                              /dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNh:        \n"
           "                                                            /mMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh:      \n"
           "                                                          -hMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMs     \n"
           "                                                         +NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMd.   \n"
           "                                                         MMMMMMM                                  MMMMMMd   \n"
           "                                                        oMMMMMM  Mi mas sentido pesame " << j1Nom <<"   MMMMMMs  \n"
           "                                                        yMMMMMMm      Has caido en desgracia      mMMMMMMm  \n"
           "                                                        hMMMMMMMm                                mMMMMMMMm  \n"
           "                                                        dMMMmmNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNmmMMMd  \n"
           "                                                        hMMM:.-/ydNMMMMMMMMMMMMMMMMMMMMMMMMMMMMmho:...NMMs  \n"
           "                                                        sMMMo     -/ydNMMMMMMMMMMMMMMMMMMMMMms:.     .MMM:  \n"
           "                                                        :MMMN-       .-+ymMMMMMMMMMMMMMMNds:.        yMMm   \n"
           "                                                         dMMMd-          .:ohmMMMMMMMmy+-          .sMMM+   \n"
           "                                                         -dMMMNhs+/:-.    .-:sNMMMMMh+-.    ..-:/ohmMMMm    \n"
           "                                                          .sNMMMMMMNNmddddmNMMMmhMmMMMNmddddmmNMMMMMMMMo    \n"
           "                                                           -sMMMMMMMMMMMMMMMMMm sM yMMMMMMMMMMMMMMMMMMM-    \n"
           "                                                          .NMMMMMMMMMMMMMMMMMm  NM  sMMMMMMMMMMMMMMMMMM:    \n"
           "                                                          :MMMMMMMMMmdysMMMMMNomMMN/hMMMMMMMmhssyNMMMMMo    \n"
           "                                                          .NMMMMMNy/.   mMMMMMMMMMMMMMMMMMMm.    sMMMNo     \n"
           "                                                           `:dmhds.    dMMMMMMMMMMMMNNMMMMo      omdo-      \n"
           "                                                                        hMMMNMMMMdMMM/yMMMN.                \n"
           "                                                                        /MMMh MMm.MMM:MMMMs                 \n"
           "                                                                        .MMM  MMh MMM MMMN                  \n"
           "                                                                         NMM  MMh MMN MMMh                  \n"
           "                                                                         mMM  MMh MMm MMMy                  \n"
           "                                                                         dMM  MMh MMm MMMy                  \n"
           "                                                                         hMM  MMh MMd MMMs                  \n"
           "                                                                         yMM  MMh MMd MMM+                  \n"
           "                                                                         oMM  MMh MMy MMM-                  \n"
           "                                                                         :MM  MMh MMo MMm                   \n"
           "                                                                          :s: NNy md. dy-                   \n";
        Sleep(200);
#ifdef Color
        }
#endif
    }
    if(evento == 50)
    {
    system("color 04");
    cout << "\n\n\n\n\n\n\n\n\n";
    cout << "                                                                      ./oyhdmmNNNNNNmdhs+:                  \n"
            "                                                                   /ymMMMMMMMMMMMMMMMMMMMMMms/.             \n"
            "                                                                :yNMMMMMMMMMMMMMMMMMMMMMMMMMMMMd+.          \n"
            "                                                              /dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNh:        \n"
            "                                                            /mMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh:      \n"
            "                                                          -hMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMs     \n"
            "                                                         +NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMd.   \n"
            "                                                         MMMMMMM                                  MMMMMMd   \n"
            "                                                        oMMMMMM      No has estado a la altura     MMMMMMs  \n"
            "                                                        yMMMMMMm          de esta batalla         mMMMMMMm  \n"
            "                                                        hMMMMMMMm                                mMMMMMMMm  \n"
            "                                                        dMMMmmNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNmmMMMd  \n"
            "                                                        hMMM:.-/ydNMMMMMMMMMMMMMMMMMMMMMMMMMMMMmho:...NMMs  \n"
            "                                                        sMMMo     -/ydNMMMMMMMMMMMMMMMMMMMMMms:.     .MMM:  \n"
            "                                                        :MMMN-       .-+ymMMMMMMMMMMMMMMNds:.        yMMm   \n"
            "                                                         dMMMd-          .:ohmMMMMMMMmy+-          .sMMM+   \n"
            "                                                         -dMMMNhs+/:-.    .-:sNMMMMMh+-.    ..-:/ohmMMMm    \n"
            "                                                          .sNMMMMMMNNmddddmNMMMmhMmMMMNmddddmmNMMMMMMMMo    \n"
            "                                                           -sMMMMMMMMMMMMMMMMMm sM yMMMMMMMMMMMMMMMMMMM-    \n"
            "                                                          .NMMMMMMMMMMMMMMMMMm  NM  sMMMMMMMMMMMMMMMMMM:    \n"
            "                                                          :MMMMMMMMMmdysMMMMMNomMMN/hMMMMMMMmhssyNMMMMMo    \n"
            "                                                          .NMMMMMNy/.   mMMMMMMMMMMMMMMMMMMm.    sMMMNo     \n"
            "                                                           `:dmhds.    dMMMMMMMMMMMMNNMMMMo      omdo-      \n"
            "                                                                        hMMMNMMMMdMMM/yMMMN.                \n"
            "                                                                        /MMMh MMm.MMM:MMMMs                 \n"
            "                                                                        .MMM  MMh MMM MMMN                  \n"
            "                                                                         NMM  MMh MMN MMMh                  \n"
            "                                                                         mMM  MMh MMm MMMy                  \n"
            "                                                                         dMM  MMh MMm MMMy                  \n"
            "                                                                         hMM  MMh MMd MMMs                  \n"
            "                                                                         yMM  MMh MMd MMM+                  \n"
            "                                                                         oMM  MMh MMy MMM-                  \n"
            "                                                                         :MM  MMh MMo MMm                   \n"
            "                                                                          :s: NNy md. dy-                   \n";
         Sleep(200);
    }
}

char Interfaz::showMessage(short evento){

    system("cls");      //Limpia la pantalla
    //system("mode con: cols=160 lines=60");      //Setea el tamaño de la consola
    Image();
    bool notificar = true;

    if(evento==11){

    //    Image();
        system("color 04");
        gotoxy(69,24);
        cout << "Te ves mal " << j1Nom;
        gotoxy(71,26);
        cout <<"Descansa un turno";
        gotoxy(2, 50);
        Sleep(1500);

    }else if(evento==12){

      //      Image();
            system("color 04");
            gotoxy(63, 24);
            cout << "Tu oponente ha perdido un turno" << endl;
            gotoxy(2,50);
            Sleep(1500);

    }else if(evento==151){

     //       Image();
            system("color 03");
            gotoxy(61,24);
            cout << "Un dragon amigo se encontro contigo...";

            Sleep(1500);
            system("color 03");
            gotoxy(61,26);
            cout << "Subete a el para avanzar 5 casilleros";
            //Image();
            system("color 03");
            gotoxy(73, 28);
            cout << j1Nom << " +5" << endl;
            gotoxy(2, 50);

    }else if(evento==150){

    //        Image();
            gotoxy(63,24);
            cout << "Te has encontrado un dragon enemigo";
            gotoxy(72,26);
            cout << "Pelea con el...";
            gotoxy(2,50);

            Sleep(1500); //Espera 1500 milisegundos antes de continuar
            system("cls");
            Image();
            system("color 04");
            gotoxy(63, 24);
            cout << "Mala suerte, te ha golpeado fuerte";
            gotoxy(59, 26);
            cout << "Retrocede 5 posiciones, o hasta el inicio";
            //Image();
            gotoxy(73, 28);
            cout << j1Nom << " -5" << endl;
            gotoxy(2,50);

    }else if(evento==251){

     //       Image();
            gotoxy(67,24);
            system("color 04");
            cout << "Oh vaya";
            gotoxy(60, 26);
            cout << "tu contrincante encontro un amigo";
            //Image();
            gotoxy(73, 28);
            cout << "Jugador 2 +5" << endl;
            gotoxy(2,50);
    }else if(evento==250){

     //       Image();
            gotoxy(67,24);
            //system("color 04");
            cout << "Al parecer tienes un amigo";

            Sleep(2000);
            Image();
            system("color 03");
            gotoxy(59, 26);
            cout << "Tu oponente ha sido golpeado por un dragon";
            gotoxy(73, 28);
            cout << "Jugador 2 -5";
            gotoxy(2,50);

    }
    else if (evento == 21 || evento == 22 || evento == 31 || evento == 32)
    {
        GameOver(evento);       //Invoca el metodo para el fin de juego
        gotoxy(77, 48);     //*
        cout <<"S - SALIR.";
        gotoxy(78, 44);     //*
        //char opcion = getch();
        gotoxy(2,50);       //Posiciona el cursor fuera de la imagen de dragones
        //return opcion;
        notificar = false;
    }
    else if (evento == 0)
    {
        system("color 02");
        gotoxy(65,24);
        cout << "Avanzas a paso firme " << j1Nom;
        gotoxy(73,26);
        cout << "Sigue para ganar";
        gotoxy(2, 50);
        Sleep(500);
    }
    if(notificar)
    {
        gotoxy(73, 36);     //*
        cout <<"ENTER - SEGUIR.";
        gotoxy(77, 40);     //*
        cout <<"S - SALIR.";
        gotoxy(78, 44);     //*
        //char opcion = getch();
        gotoxy(2,50);       //Posiciona el cursor fuera de la imagen de dragones
        //return opcion;
    }
    char aux = getch();
    bool seguir = (aux != 's' && aux != 'S');
    if (!seguir)
    {
        system("cls");      //Limpia la pantalla
        if(evento != 31 && evento != 21 && evento != 22 && evento != 32)
        {
            system("color 04");
            GameOver(50);
        }
    }
    return seguir;
}
string Interfaz::setName(){

   string _name;

   system("color 02");      //Cambia el color del texto y del fondo

   Image();     //Invoca a la imagen de los dragones
   gotoxy(65,24);      //Posiciona el cursor para escribir la pregunta en medio de los dragones
   cout <<"Cual es tu nombre guerrero?...";
   gotoxy(75,28);      //Posiciona el cursor debajo de la pregunta
   cin >> _name;       //Lee el nombre del jugador

   gotoxy(2,50);       //Coloca el cursor fuera de la imagen

   j1Nom = _name;

   return _name;        //Retorna el nombre del jugador 1

}

void Interfaz::clear()
{
    system("cls");
}

void Interfaz::gotoxy(short x, short y){
    COORD cp = {x, y};
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), cp);
}




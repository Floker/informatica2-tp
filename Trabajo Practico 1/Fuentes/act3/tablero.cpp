/**
 * @file tablero.cpp
 * @author Loker Federico (federicoloker@alu.frp.utn.edu.ar)
 * @brief Este archivo implementa la clase tablero
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */

#include "tablero.h"


short Tablero::getAleatorio()
{
    short num;
    num = 3 + rand() % (47);
    return num;
}

Tablero::Tablero()
{

}

void Tablero::inicializar(Jugador *j1, Jugador *j2)
{
    jugador1 = j1;
    jugador2 = j2;
    srand(time(NULL));
    jugador1->setPos(1);
    jugador1->setHab(1);
    jugador2->setPos(1);
    jugador2->setHab(1);
}

short Tablero::jugarCiclo()
{
    short j1pos = jugador1->getPos() + dado.tirarDado();
    short j2pos = jugador2->getPos() + dado.tirarDado();
    jugador1->setPos(j1pos);
    jugador2->setPos(j2pos);

    short evento = hayGanador();

    return evento;
}

short Tablero:: hayGanador()
{
    short aux=0;

    if(jugador1->getPos() >= 50)
        aux = 21;
    else if (jugador2->getPos() >= 50)
        aux = 22;

    return aux;
}

short Tablero::aplicarReglas()
{
return 0;
}

Tablero::~Tablero(){

}

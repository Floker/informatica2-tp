/**
 * @file consola.c
 * @author Bertolin Gianfranco (gianfrancobertolin@alu.frp.utn.edu.ar)
 * @brief Este archivo implementa la clase consola que muestra el informe de la aplicacion por la consola
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */
#include <stdio.h>

void consola(double aux, double integral, int iteraciones, int intervalos){

    printf("\t\t\t\tEjercicio 1\n");
    printf("Regla de Simpson 1/3:\n");
    printf("\n\tTrabajo realizado: %.12lf [W]",integral);
    printf("\n\tCantidad de sub-intervalos tomados: %d\n",intervalos);
    printf("\tError medido: %.12lf \n", aux);
    printf("\tIteraciones hasta alcanzar el error deseado: %d\n\n", iteraciones);

}

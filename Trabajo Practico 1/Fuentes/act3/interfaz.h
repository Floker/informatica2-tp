/**
 * @file interfaz.h
 * @author D'angelo Pablo (pablodangelomejias@alu.frp.utn.edu.ar)
 * @brief Este header define la clase de la interfaz de consola
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */

#ifndef INTERFAZ_H
#define INTERFAZ_H

#include <string>
#include <conio.h>

using namespace std;

class Interfaz
{
protected:

    /*!
     * \brief j1Nom: nombre del jugador
     */
    string j1Nom;

    /*!
     * \brief Image: Tiene cargado la imagen que se mostrara en los menus.
     */
    void Image();

    /*!
     * \brief GameOver: Muestra un mensaje de "Fin del Juego".
     *                  Recibe un parametro short, que indica que jugador gano y de que manera.
     */
    void GameOver(short);

    /*!
     * \brief gotoxy: posiciona el cursor en las coordenadas dadas
     * \param x: coordenada en x de la consola
     * \param y: coordenada en y de la consola
     */
    void gotoxy(short x, short y);

public:

    /*!
     * \brief Interfaz: Construye una interfaz
     */
    Interfaz();

    /*!
    * \brief Welcome: Muestra una imagen de bienvenida y la opcion de jugar o salir.
    * \return Retorna una opcion para salir o comenzar el juego.
    */
    short Welcome();

    /*!
    * \brief Menu: Muestra un menu para seleccionar el tablero.
    * \return Retorna la opcion.
    */
    short Menu();

    /*!
    * \brief showMessage: Muestra con mensajes de que manera va avanzando el juego.
    * \return 1 para seguir, 0 para salir.
    */
    char showMessage(short);

    /*!
    * \brief setName: Muestra una interfaz solicitando el nombre del jugador.
    * \return Retorna el nombre del jugador.
    */
    string setName();

    /*!
    * \brief clear: Limpia la pantalla
    */
    void clear();
};

#endif // INTERFAZ_H

/**
 * @file archivator.h
 * @author Loker Federico (federicoloker@alu.frp.utn.edu.ar)
 * @brief Este header define la clase archivator
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */

#ifndef ARCHIVATOR_H
#define ARCHIVATOR_H

#include "eqsys.h"
#include <string>
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

using namespace std;

class archivator
{
public:
    archivator();

    short readFile(eqSys * sys, string path);
    short writeFile(eqSys * sys, string path);

};

#endif // ARCHIVATOR_H

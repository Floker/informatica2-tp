/**
 * @file tablero.cpp
 * @author Balaudo Tomas (tomasbalaudo@alu.frp.utn.edu.ar)
 * @brief Este archivo implementa la clase de tablero experto que es un modo de juego
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */

#include "texperto.h"

tExperto::tExperto()
{

}

void tExperto::inicializar(Jugador *j1, Jugador *j2)
{
    jugador1 = j1;
    jugador2 = j2;
    srand(time(NULL));
    jugador1->setPos(1);
    jugador1->setHab(1);
    jugador2->setPos(1);
    jugador2->setHab(1);

    for(short i = 0; i<2; i++)
    {
        dragonesJ1[i].setDuenio(jugador1);
        dragonesJ1[i].posicionar(getAleatorio());
        dragonesJ1[i].setInc(1);

        dragonesJ2[i].setDuenio(jugador2);
        dragonesJ2[i].posicionar(getAleatorio());
        dragonesJ2[i].setInc(1);
    }

    calabozos[0].posicionar(getAleatorio());
    calabozos[0].setInc(1);
    calabozos[1].posicionar(getAleatorio());
    calabozos[1].setInc(1);
    calabozos[2].posicionar(getAleatorio());
    calabozos[2].setInc(1);

}

short tExperto::aplicarReglas()
{
    short aux=0;
    // JUGADOR 1
    bool j1d10= dragonesJ1[0].getPos() == jugador1->getPos();//Posicion dragonJ1[0] y posicion de J1
    bool j1d11= dragonesJ1[1].getPos() == jugador1->getPos();//Posicion dragonJ1[0] y posicion de J1
    /////////////////////////////////////////////////////////
    bool j1d20=dragonesJ2[0].getPos() == jugador1->getPos(); //Posicion dragonJ2[0] y posicion de J1
    bool j1d21= dragonesJ2[1].getPos() == jugador1->getPos();//Posicion dragonJ2[1] y posicion de J1
    ////////////////////////////////////////////////////////
    bool calabozoj1=calabozos[0].getPos() == jugador1->getPos()||calabozos[1].getPos() == jugador1->getPos()||calabozos[2].getPos()==jugador1->getPos(); //Jugador 1 en calabozo
    ///////////////////////////////////////////////////////
    /// Jugador 1 cae en un calabozo con un dragón suyo
    bool j1d10c0= dragonesJ1[0].getPos() == jugador1->getPos() && calabozos[0].getPos()==jugador1->getPos();
    bool j1d10c1= dragonesJ1[0].getPos() == jugador1->getPos() && calabozos[1].getPos()==jugador1->getPos();
    bool j1d10c2= dragonesJ1[0].getPos() == jugador1->getPos() && calabozos[2].getPos()==jugador1->getPos();

    bool j1d11c0= dragonesJ1[1].getPos() == jugador1->getPos() && calabozos[0].getPos()==jugador1->getPos();
    bool j1d11c1= dragonesJ1[1].getPos() == jugador1->getPos() && calabozos[1].getPos()==jugador1->getPos();
    bool j1d11c2= dragonesJ1[1].getPos() == jugador1->getPos() && calabozos[2].getPos()==jugador1->getPos();
    //////////////////////////////////////////////////////
    /// Jugador 1 cae en un calabozo con un dragón rival
    bool j1d20c0= dragonesJ2[0].getPos() == jugador1->getPos() && calabozos[0].getPos()==jugador1->getPos();
    bool j1d20c1= dragonesJ2[0].getPos() == jugador1->getPos() && calabozos[1].getPos()==jugador1->getPos();
    bool j1d20c2= dragonesJ2[0].getPos() == jugador1->getPos() && calabozos[2].getPos()==jugador1->getPos();

    bool j1d21c0= dragonesJ2[1].getPos() == jugador1->getPos() && calabozos[0].getPos()==jugador1->getPos();
    bool j1d21c1= dragonesJ2[1].getPos() == jugador1->getPos() && calabozos[1].getPos()==jugador1->getPos();
    bool j1d21c2= dragonesJ2[1].getPos() == jugador1->getPos() && calabozos[2].getPos()==jugador1->getPos();

    //JUGADOR 2
    bool j2d20= dragonesJ2[0].getPos() == jugador2->getPos();//Posicion dragonJ2[0] y posicion de J2
    bool j2d21= dragonesJ2[1].getPos() == jugador2->getPos();//Posicion dragonJ2[0] y posicion de J2
    /////////////////////////////////////////////////////////
    bool j2d10=dragonesJ1[0].getPos() == jugador2->getPos(); //Posicion dragonJ1[0] y posicion de J2
    bool j2d11= dragonesJ1[1].getPos() == jugador2->getPos();//Posicion dragonJ1[1] y posicion de J2
    ////////////////////////////////////////////////////////
    ///Jugador 2 en calabozo
    bool calabozoj2= calabozos[0].getPos() == jugador2->getPos()||calabozos[1].getPos() == jugador2->getPos()||calabozos[2].getPos()==jugador2->getPos(); //Jugador 2 en calabozo
    ///////////////////////////////////////////////////////
    /// Jugador 2 cae en un calabozo con un dragón suyo
    bool j2d20c0= dragonesJ2[0].getPos() == jugador2->getPos() && calabozos[0].getPos()==jugador2->getPos();
    bool j2d20c1= dragonesJ2[0].getPos() == jugador2->getPos() && calabozos[1].getPos()==jugador2->getPos();
    bool j2d20c2= dragonesJ2[0].getPos() == jugador2->getPos() && calabozos[2].getPos()==jugador2->getPos();

    bool j2d21c0= dragonesJ2[1].getPos() == jugador2->getPos() && calabozos[0].getPos()==jugador2->getPos();
    bool j2d21c1= dragonesJ2[1].getPos() == jugador2->getPos() && calabozos[1].getPos()==jugador2->getPos();
    bool j2d21c2= dragonesJ2[1].getPos() == jugador2->getPos() && calabozos[2].getPos()==jugador2->getPos();
    //////////////////////////////////////////////////////
    /// Jugador 2 cae en un calabozo con un dragón rival
    bool j2d10c0= dragonesJ1[0].getPos() == jugador2->getPos() && calabozos[0].getPos()==jugador2->getPos();
    bool j2d10c1= dragonesJ1[0].getPos() == jugador2->getPos() && calabozos[1].getPos()==jugador2->getPos();
    bool j2d10c2= dragonesJ1[0].getPos() == jugador2->getPos() && calabozos[2].getPos()==jugador2->getPos();

    bool j2d11c0= dragonesJ1[1].getPos() == jugador2->getPos() && calabozos[0].getPos()==jugador2->getPos();
    bool j2d11c1= dragonesJ1[1].getPos() == jugador2->getPos() && calabozos[1].getPos()==jugador2->getPos();
    bool j2d11c2= dragonesJ1[1].getPos() == jugador2->getPos() && calabozos[2].getPos()==jugador2->getPos();


    if(dragonesJ1[0].getInc()||dragonesJ1[1].getInc())
   {

        if(j1d10||j1d11)
        {
               jugador1->setPos(jugador1->getPos()+5); // Avanza 5 posiciones si se encuentra con uno de sus dragones
               aux = 151;
        }

        if (j1d20||j1d21)
        {
            jugador1->setPos(jugador1->getPos()-5); // Retrocede 5 posiciones si se encuentra con uno de los dragones rivales
            aux = 150;
        }

        if (calabozoj1)
        {
             // Pierde el turno
            aux = 11;
            jugador1->setHab(0);
        }

        if(j1d10c0||j1d10c1||j1d10c2||j1d11c0||j1d11c1||j1d11c2)
        {
                // No hay incidencia
               aux = 70;
               dragonesJ1[0].setInc(0);
               dragonesJ1[1].setInc(0);
        }

        if(j1d20c0||j1d20c1||j1d20c2||j1d21c0||j1d21c1||j1d21c2)
        {
               // Pierde el juego porque te encontraste con un calabozo
               aux = 32;
        }
    }

    if(dragonesJ2[0].getInc()&&dragonesJ2[1].getInc())
    {
        //Jugador 2
        if(j2d20||j2d21)
        {
               jugador2->setPos(jugador2->getPos()+5); // Avanza 5 posiciones si se encuentra con uno de sus dragones
               aux = 251;
        }

        if (j2d10||j2d11)
        {
            jugador2->setPos(jugador2->getPos()-5); // Retrocede 5 posiciones si se encuentra con uno de los dragones rivales
            aux = 250;
        }

        if (calabozoj2)
        {
             // Pierde el turno
            aux = 12;
            jugador2->setHab(0);
        }

        if(j2d20c0||j2d20c1||j2d20c2||j2d21c0||j2d21c1||j2d21c2)
        {
            // No hay incidencia
               aux = 71;
               dragonesJ2[0].setInc(0);
               dragonesJ2[1].setInc(0);
        }

        if(j2d10c0||j2d10c1||j2d10c2||j2d11c0||j2d11c1||j2d11c2)
        {
               // Pierde el juego porque te encontraste con un calabozo
               aux =31;
        }
    }

    // si ninguno gana por abandono, veo si alguno llego al final
    if(!aux)
        aux = hayGanador();

    return aux;
}

short tExperto::jugarCiclo()
{
    short j1pos = 0;
    short j2pos = 0;

    if (jugador1->getHab())
        j1pos = jugador1->getPos() + dado.tirarDado();
    else
        jugador1->setHab(1);

    if (jugador2->getHab())
        j2pos = jugador2->getPos() + dado.tirarDado();
    else
        jugador2->setHab(1);

    jugador1->setPos(j1pos);
    jugador2->setPos(j2pos);

    short evento = aplicarReglas(); // si evento es 22 es porque gano el jugador por que 1 calabozo

    for (int i = 0; i < N_DRAGONES; i++)
    {
        dragonesJ1[i].desplazar();
        dragonesJ2[i].desplazar();
    }
return evento;
}

tExperto::~tExperto(){

}

/**
 * @file portconf.cpp
 * @author Loker Federico (federicoloker@alu.frp.utn.edu.ar)
 * @brief Este archivo implementa la clase portconf
 * @version 0.1
 * @date 2021-11-10
 *
 * @copyright MIT Licence 2021
 *
 */


#include "portconf.h"
#include "ui_portconf.h"

portconf::portconf(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::portconf)
{
    ui->setupUi(this);
}

portconf::~portconf()
{
    delete ui;
}

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        consola.c \
        function.c \
        main.c

HEADERS += \
    consola.h \
    functions.h

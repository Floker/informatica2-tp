/**
 * @file interfaz.h
 * @author D'angelo Pablo (pablodangelomejias@alu.frp.utn.edu.ar)
 * @brief Este header define la clase de la interfaz de consola
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */

#ifndef INTERFAZ_H
#define INTERFAZ_H
#include <iostream>
#include "solver.h"
#include <string>
#include <cctype>
#include <cstdlib>

using namespace std;
class interfaz
{
protected:
    /*!
     * \brief esNumero: Verifica si en valor ingresado es un numero, es decir se encuentra dentro de los parametros requeridos
     * \param linea: Valor a verificar
     * \return Uno para "sí, es numero"
     */
    bool esNumero(string linea);

public:
    interfaz();

    /*!
     * \brief selectCarga: Permite seleccionar mediante consola el metodo de carga del sistema.
     * \return 1: Para archivos.
     *         2: Para consola.
     */
    short selectCarga();
    /*!
     * \brief cargarSys: Carga mediante consola el sistema a resolver.
     * \param sys: Sistema de ecuaciones a resolver
     * \return (parametro) el sistema cargado.
     */
    void cargarSys(eqSys *sys);
    /*!
     * \brief mostrarSys: Muestra en pantalla el sistema cargado por el usuario.
     * \param sys: Sistema de ecuaciones a resolver.
     */
    void mostrarSys(eqSys *sys);
    /*!
     * \brief selectMetodo: Permite seleccionar el metodo.
     * \param orden: Orden del sistema cargado.
     * \return 1: Resuelve con Gauss.
     *         2: Resuelve con Cramer.
     */
    short selectMetodo(unsigned int orden);
    /*!
     * \brief mostrarSol: Imprime en pantalla la solucion del sistema cargado.
     * \param sys: Sistema de ecuaciones a resolver.
     */
    void mostrarSol(eqSys *sys);
    /*!
     * \brief final: Menu de opciones que finaliza el programa o lo ejecuta nuevamente.
     * \return 1 para Realizar otra operacion.
     *         2 para Guardar resultados.
     *         3 para Salir.
     */
    short final();
    /*!
     * \brief nomArchivoGuar: Permite ingresa el nombre con el que se desea guardar el archivo del sistema resuelto.
     * \return Nombre con el que se guardará el archivo
     */
    string nomArchivoGuar();
    /*!
     * \brief nomArchivoCar: Permite ingresar el nombre del archivo que se desea cargar.
     * \return Nombre del archivo que se desea cargar.
     */
    string nomArchivoCar();
    /*!
     * \brief mostrarMensaje: Muestra en pantalla si se produjo un error durante la ejecucion del programa o da un mensaje de finalización.
     * \param error: Error procucido, identificado por un numero particular.
     *               20: No se pudo cargar archivo.
     *               21: No se pudo guardar el archivo.
     *               22: Archivo guardado con exito.
     *               30: El sistema tiene infinitas soluciones.
     *               31: El sistema es inconsistente.
     *               99: Finaliza el programa.
     */
    void mostrarMensaje(short error);
};

#endif // INTERFAZ_H


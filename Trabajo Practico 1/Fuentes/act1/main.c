/**
 * @file main.c
 * @author Bertolin Gianfranco (gianfrancobertolin@alu.frp.utn.edu.ar)
 * @brief Este es el archivo principal donde se invocan la libreria con las funciones y la consola
 * @version 0.1
 * @date 2021-07-30
 *
 * @copyright MIT Licence 2021
 *
 */
#include <stdio.h>
#include <math.h>
#include "functions.h"
#include "consola.h"

#define vreal 0.461821665154045 //valor de integral calculado mediante una calculadora cientifica
#define error  0.00000001 //error deseado
#define a 0 //extremo inferior de integracion
#define b 13 //extremo superior de integracion
#define interv 50 //incremento de intervalos

int main()
{
    int i=0 , it=0;
    double integral, aux;

    do{
       i=i+interv;
       integral = simpsons(f,a,b,i);
       it=it+1;     //contador para ver la cantidad de interaciones para llegar al error.

      }while(fabsl(((vreal - integral)/vreal)*100) >= error);   // acá se calcula el error.

    /*   En el do while se incrementan el numero de intervalos que sea necesario para llegar al error que deseamos
     *
     *
    */

    aux = fabsl(((vreal - integral)/vreal)*100);
    consola(aux, integral, it, i);

    return 0;
}




